<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'widget_css' => array(
        '//chatboxci/assets/css/style.css', 
        '//chatboxci/assets/css/bootstrap.css'
    ),
    'widget_js' => array(
        '//chatboxci/assets/js/jquery/jquery-1.7.1.min.js',
        '//chatboxci/assets/js/porthole/porthole.js', 
        '//chatboxci/assets/js/strophe/strophe.min.js',
        '//chatboxci/assets/js/strophe/strophe.xdomainrequest.js',
        '//chatboxci/assets/js/jpaq/jpaq.min.js', 
        '//chatboxci/assets/js/bootstrap.min.js', 
        '//chatboxci/assets/js/jquery/jquery.watermark.min.js', 
        '//chatboxci/assets/js/jquery/jquery.cookie.js', 
        '//chatboxci/assets/js/wgchat/wgchatapi2.js'
    ),
    'widget_flxhr_js' => array(
        '//chatboxci/assets/js/porthole/porthole.js', 
        '//chatboxci/assets/js/strophe/strophe.min.js', 
        '//chatboxci/assets/js/strophe/flensed.js', 
        '//chatboxci/assets/js/strophe/flXHR.js', 
        '//chatboxci/assets/js/strophe/strophe.flxhr.js', 
        '//chatboxci/assets/js/jpaq/jpaq.min.js', 
        '//chatboxci/assets/js/jquery/jquery-1.7.1.min.js', 
        '//chatboxci/assets/js/bootstrap.min.js', 
        '//chatboxci/assets/js/jquery/jquery.watermark.min.js', 
        '//chatboxci/assets/js/jquery/jquery.cookie.js', 
        '//chatboxci/assets/js/wgchat/wgchatapi2.js'
    ),
);