<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
		<script>
			var oauth_url = 'https://www.facebook.com/dialog/oauth/';
			oauth_url += '?client_id=<?php echo $app_id; ?>';
			oauth_url += '&redirect_uri=' + encodeURIComponent('<?php echo $callback_url; ?>');
			oauth_url += '&scope=email'

			redirect_auth = function() {
				window.top.location = oauth_url;
			};
		</script>
	</head>
	<body>
		<div class="hero-unit">
			<h1>Authorization Needed</h1>
			<p>Please authorize this application to start live chatting with us :)</p>
			<p><a class="btn btn-primary btn-large" onclick="redirect_auth()">Authorize Now</a></p>
		</div>
	</body>
</html>