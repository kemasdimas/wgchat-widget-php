<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<title>WGchat Widget Added</title>
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
	</head>
	<body>
		<div class="hero-unit">
			<h1>WGchat Widget Added!</h1>
			<p>You have added WGchat Live Chat Widget to your page(s), click button(s) below to visit your page:</p>
			<?php foreach ($pages as $page_id => $value) : ?>
				<a href="<?php echo "https://www.facebook.com/pages/null/$page_id/?sk=app_$app_id" ?>" target="_blank" class="btn btn-primary">Page ID #<?php echo $page_id; ?></a>
			<?php endforeach; ?>
			<p>* you can only use 1 API key in 1 facebook widget :)</p>
		</div>
	</body>
</html>