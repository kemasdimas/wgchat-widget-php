<!DOCTYPE HTML>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		
		<meta charset="utf-8">
		<title><?php echo $identityHeaderText; ?></title>
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
		
		<script type="text/javascript">
			var WGchat_GLOBAL = function() {
				return {
					apiKey: '<?php echo $apiKey; ?>',
					parentUrl: '<?php echo $parentUrl; ?>',
					widgetTitle: '<?php echo $widgetTitle; ?>',
					workgroup: '<?php echo $workgroupJid; ?>',
					userID: '<?php echo $userID; ?>',
					boshUrl: '<?php echo $boshUrl; ?>',
					xmppDomain: '<?php echo $xmppDomain; ?>',
					baseUrl: '<?php echo base_url(); ?>',
					isWidget: <?php echo ($isWidgetized == TRUE) ? 'true' : 'false'; ?>,
					channelId: '<?php echo isset($channelId) ? $channelId : '' ?>',
					colorScheme: <?php echo $colorScheme ?>,
					userData: <?php echo isset($user_json) ? $user_json : 'null'  ?>
				};
			}
		</script>
		
		<script type="text/javascript">
		  var customPath = 'chat';
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?php echo $global_ga_account; ?>']);
		  _gaq.push(['_setDomainName', 'none']);
		  _gaq.push(['_setCookiePath', window.location.pathname]);
		  
		  _gaq.push(['acc2._setAccount', '<?php echo $client_ga_account; ?>']);
		  _gaq.push(['acc2._setDomainName', 'none']);
		  _gaq.push(['acc2._setCookiePath', window.location.pathname]);
		  
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	</head>
	<body>
		<?php echo $content; ?>
		
		<script type="text/javascript" src="<?php echo site_url('min/g=widget_js'); ?>"></script>
	</body>
</html>