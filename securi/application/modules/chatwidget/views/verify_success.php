<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
	</head>
	<body>
		<div class="hero-unit">
			<h1>Verified!</h1>
			<p>Congratulation, you have verified your <strong>WGchat live chat widget</strong> on Facebook. You can now refresh this page :)</p>
		</div>
	</body>
</html>