<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
	</head>
	<body>
		<div class="hero-unit">
			<p>Please enter your API key</p>
			<?php echo form_open(site_url('chatwidget/verify_facebookwidget/' . $page_id)); ?>
				<?php echo form_input('api_key', set_value('api_key'), 'class="input-xlarge"') ?><br />
				<?php echo form_error('api_key', '<div class="alert alert-error">', '</div>'); ?>
				<span>You can get your API key in <a href="http://manage.wgchat.com/">WGchat Control Panel</a></span>
				<p><?php echo form_submit('verify', 'Verify Now', 'class="btn btn-primary btn-large"') ?></p>
			<?php echo form_close(); ?>
		</div>
	</body>
</html>