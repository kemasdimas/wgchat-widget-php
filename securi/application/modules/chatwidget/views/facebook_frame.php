<div id="fb-root" style="display: none;"></div>
<script>
    window.fbAsyncInit = function() {
		FB.init({
	        appId: '<?php echo $app_id; ?>', 
	        status: true,
	        cookie: true,
	        oauth: true
	    });
	    
	    FB.Canvas.setSize({ height: 452 });
	};
	
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	    if (d.getElementById(id)) {return;}
	    js = d.createElement('script'); js.id = id; js.async = true;
	    js.src = "//connect.facebook.net/en_US/all.js";
	    ref.parentNode.insertBefore(js, ref);
	}(document));
</script>
<div id="extra-container" style="height: 450px; border-bottom-width: 1px;">
	<div>
		<?php echo $chat_area; ?>
	</div>
</div>