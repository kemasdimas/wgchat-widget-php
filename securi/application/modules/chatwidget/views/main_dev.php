<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo $identityHeaderText; ?></title>
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/css/style.css'); ?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.css'); ?>" />
		
		<script type="text/javascript">
			var WGchat_GLOBAL = function() {
				return {
					apiKey: '<?php echo $apiKey; ?>',
					parentUrl: '<?php echo $parentUrl; ?>',
					widgetTitle: '<?php echo $widgetTitle; ?>',
					workgroup: '<?php echo $workgroupJid; ?>',
					userID: '<?php echo $userID; ?>',
					boshUrl: '<?php echo $boshUrl; ?>',
					xmppDomain: '<?php echo $xmppDomain; ?>',
					baseUrl: '<?php echo base_url(); ?>',
					isWidget: <?php echo ($isWidgetized == TRUE) ? 'true' : 'false'; ?>,
					channelId: '<?php echo isset($channelId) ? $channelId : '' ?>',
					colorScheme: <?php echo $colorScheme ?>,
					userData: <?php echo isset($user_json) ? $user_json : 'null'  ?>
				};
			}
		</script>
		
		<script type="text/javascript">
		  var customPath = 'chat';
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', '<?php echo $global_ga_account; ?>']);
		  _gaq.push(['_setDomainName', 'none']);
		  _gaq.push(['_setCookiePath', window.location.pathname]);
		  
		  _gaq.push(['acc2._setAccount', '<?php echo $client_ga_account; ?>']);
		  _gaq.push(['acc2._setDomainName', 'none']);
		  _gaq.push(['acc2._setCookiePath', window.location.pathname]);
		  
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>
	</head>
	<body>
		<?php echo $content; ?>

		<script type="text/javascript" src="<?php echo site_url('assets/js/strophe/strophe.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/porthole/porthole.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/jpaq/jpaq.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/jquery/jquery-1.7.1.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/jquery/jquery.watermark.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/jquery/jquery.cookie.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo site_url('assets/js/wgchat/wgchatapi2.js'); ?>"></script>
		
		<script type="text/javascript" src="<?php echo site_url('assets/js/strophe/strophe.xdomainrequest.js'); ?>"></script>
	</body>
</html>