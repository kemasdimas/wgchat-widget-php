<div id="header">
	<span class="text"><?php echo $identityHeaderText; ?></span>
	<span class="right">
		<?php if ($isWidgetized == TRUE) : ?>
			<a href="javascript:void(null);" class="btn-header" id="btn-hide-chat" title="Hide Chat"><i class="icon-chevron-down icon-white"></i></a>
		<?php endif; ?>
		<a href="javascript:void(null);" class="btn-header btn btn-danger btn-mini hidden" id="btn-close-chat" title="End Chat">End Chat</a>
	</span>
	<div class="clearfix"></div>
</div>
<div id="container">
	<div id="offline-form" class="hidden">
		<form>
        <fieldset>
          <div class="control-group">
            <div class="controls">
              <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i></span><input class="span2" id="offline-username" type="text" placeholder="Name">
              </div>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <div class="input-prepend">
                <span class="add-on"><i class="icon-envelope"></i></span><input class="span2" id="offline-email" type="text" placeholder="Email address">
              </div>
            </div>
          </div>
          <div class="control-group" style="margin-bottom: 0px !important;">
            <div class="controls">
              <textarea class="span3" style="margin-bottom: 0px !important;" id="offline-message" placeholder="<?php echo $offlineText; ?>" rows="3"></textarea>
            </div>
          </div>
          <button id="offline-send" type="submit" class="btn btn-primary" data-loading-text="Sending message...">Send Offline Message</button>
        </fieldset>
      </form>
	</div>
	<div id="loading-indicator" style="padding-top: 20px;">
		<span class="center"><img src="<?php echo site_url('assets/img/loader.gif'); ?>" alt="Loading..." /></span>
		<span id="loading-text" class="center">Please wait...</span>
	</div>
	<div id="form-area" class="hidden">
	</div>
	<div id="chat-intro" class="hidden">
		<p><?php echo $onlineText; ?></p>
		<a class="btn btn-large btn-primary" id="btn-start-chat" href="javascript:void(null);">Start Chat</a>
	</div>
	<div id="chat-area" class="hidden">
		<div id="inner-chat-area"></div>
	</div>
	<div id="chat-input-area" class="hidden">
		<textarea id="txt-chat-input" name="txt-chat-input"></textarea>
	</div>
	<div class="clearfix"></div>
</div>
<div id="footer" style="<?php echo (isset($footer_style)) ? $footer_style : ''; ?>">
	<span id="wgchat-watermark">
		<?php if (isset($no_ads) && $no_ads == 1) : ?>
			&nbsp;
		<?php else : ?>
			<a href="http://wgchat.com/" target="_blank">Powered by <b>WGchat.com</b></a>
		<?php endif; ?>
	</span>
</div>