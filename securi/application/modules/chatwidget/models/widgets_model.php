<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widgets_model extends CI_Model {
	
	private $table_name = 'widgets';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_widget($api_key, $host_url, $preview = FALSE)
	{
		$no_www = preg_replace('/^www\.(.+\.)/', '$1', $host_url);
		
		$query = $this->db
			->select('users.wg_name, users.XMPP_SERVER_URL, users.BOSH_URL, users.ga_account_id, widgets.*, no_ads, ovr_show_widget_ads, widget_size')
			->join('users', 'users.id = widgets.id_user')
			->join('feature_rules', 'feature_rules.id = users.paket_id')
			->join('feature_overrides', 'feature_overrides.user_id = users.id', 'left')
			->where('api_key', $api_key);
		
		if ($preview == FALSE) {
			if (strpos($no_www, 'facebook.com') != FALSE) {
				$query = $query
					->where('widgets.url', 'facebook.com');
			} else {
				$query = $query
					->where('widgets.url', $no_www)
					->or_where('widgets.url', 'www.'.$no_www);	
			}
		}
		
		$query = $query->get($this->table_name);
		
		return $query->row_array();
	}
	
	public function verify_facebook_widget($api_key, $page_id)
	{
		$query = $this->db
			->where('api_key', $api_key)
			->where('facebook_page_id IS NULL')
			->or_where('facebook_page_id', '')
			->where('is_facebook', 1)
			->get($this->table_name);
		
		$available_widget = $query->row_array();
		
		$result = FALSE;
		if (count($available_widget) > 0) {
			$available_widget['facebook_page_id']	= $page_id;
			$available_widget['url']				= 'facebook.com';
			
			$result = $this->db
				->where('id', $available_widget['id'])
				->update($this->table_name, $available_widget);
		}

		return $result;
	}
	
	public function get_facebook_widget_key($page_id)
	{
		$query = $this->db
			->select('api_key')
			->where('is_facebook', 1)
			->where('facebook_page_id', $page_id)
			->limit(1)
			->get($this->table_name);
		
		return $query->row_array();
	}
}
