<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chatwidget extends MX_Controller {

	private $emailSender = 'no-reply-widget@wgchat.com';

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Widgets_model');
		$this->load->config('wgchat');
	}

	private function _send_email($destination, $message, $subject, $my_name, $reply_to = NULL, $name = 'Customer')
	{
		if (ENVIRONMENT != 'development') {
			$this->load->library('email');
			
			$this->email->to($destination);
		    $this->email->from($this->emailSender, $my_name);
		    $this->email->subject($subject);
		    $this->email->message($message);
			
			if ($reply_to != NULL) {
				$this->email->reply_to($reply_to, $name);
			}
			
		    return $this->email->send();
		}
		
		return TRUE;
	}

	private function _authorize_widget($api_key, $complete_url)
	{
		$host_url 		= parse_url($complete_url, PHP_URL_HOST);
		$preview_url	= parse_url(base_url(), PHP_URL_HOST);
		
		$preview = $host_url == $preview_url;
		
		$this->load->model('chatwidget/Widgets_model');
		$widget_data = $this->Widgets_model->get_widget($api_key, $host_url, $preview);
		
		if (count($widget_data) == 0) {
			$this->output->set_status_header('401');
			$this->output->set_output('Not Allowed! Make sure you use exact API Key for this website. Your url now is: ' . $complete_url);
		}
		
		return $widget_data;	
	}	
	
	private function _prepare_frame($api_key, $referrer)
	{
		$autorize_result = $this->_authorize_widget($api_key, $referrer);
		
		if (count($autorize_result) == 0) {
			return FALSE;
		}
		
		// Determine to show ads / not
		$no_ads = $autorize_result['no_ads'];
		if ($autorize_result['ovr_show_widget_ads'] != NULL && $autorize_result['ovr_show_widget_ads'] == 1) {
			$no_ads = $autorize_result['no_ads'] = 0;
		}
		
		$session_id = $this->session->userdata('session_id');
		$data = array(
			'apiKey'	=> $api_key,
			'parentUrl' => parse_url($this->agent->referrer(), PHP_URL_HOST),
			
			'widgetTitle' 	=> $autorize_result['widget_title'],
			'onlineText'	=> $autorize_result['online_text'],
			'workgroupJid' 	=> $autorize_result['wg_name'] . '@workgroup.' . $autorize_result['XMPP_SERVER_URL'],
			'userID' 		=> $session_id,
			
			'identityHeaderText' 	=> $autorize_result['header_text'],
			'identityHeaderImage'	=> $autorize_result['header_logo'],
			
			'boshUrl' 		=> $autorize_result['BOSH_URL'],
			'xmppDomain' 	=> $autorize_result['XMPP_SERVER_URL'],
			'offlineText'	=> 'We are currently offline, type your message here.',
			'colorScheme'	=> $this->config->item('wgchat_default_color_scheme'),
			'widgetSize'	=> $autorize_result['widget_size'],
			
			'global_ga_account'	=> $this->config->item('wgchat_global_ga_account'),
			'client_ga_account' => 'UA-33534337-2',
			
			'no_ads'	=> $no_ads
		);
		
		// channel ID for custom routing
		if ($autorize_result['channel_id'] != NULL && strlen(trim($autorize_result['channel_id'])) > 0)  {
			$data['channelId'] = $autorize_result['channel_id'];
		}
		
		// offline text shown when workgroup is offline
		if ($autorize_result['offline_text'] != NULL && strlen(trim($autorize_result['offline_text'])) > 0)  {
			$data['offlineText'] = $autorize_result['offline_text'];
		}
		
		// color scheme for the widgets
		if ($autorize_result['color_scheme'] != NULL && strlen(trim($autorize_result['color_scheme'])) > 0)  {
			$data['colorScheme'] = $autorize_result['color_scheme'];
		}
		
		// client google analytics id
		if ($autorize_result['ga_account_id'] != NULL && strlen(trim($autorize_result['ga_account_id'])) > 0) {
			$data['client_ga_account'] = $autorize_result['ga_account_id'];
		}
		
		return $data;
	}
	
	public function offline_message()
	{
		$api_key 	= $this->input->post('api_key');
		$parent_url	= $this->input->post('parent_url');
		
		if ($this->input->is_ajax_request() && $api_key != FALSE) 
		{
			$autorize_result = $this->_authorize_widget($api_key, 'http://' . $parent_url);
			
			if (count($autorize_result) == 0) {
				return;
			}
			
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('', '<br/>');
			$this->form_validation->set_rules(array(
				array(
					'field'	=> 'name',
					'rules'	=> 'trim|required|xss_clean'
				),
				array(
					'field'	=> 'email',
					'rules'	=> 'trim|required|valid_email|xss_clean'
				),
				array(
					'field'	=> 'message',
					'rules'	=> 'trim|required|xss_clean'
				)
			));
			
			if ($this->form_validation->run() == FALSE) {
				
				$this->output
					->set_status_header(400)
					->set_content_type('application/json')
					->set_output(json_encode(array(
						'status'	=> 400,
						'message'	=> validation_errors()
					)));
			} else {
				
				$this->load->helper('email');
				$admin_email = $autorize_result['offline_email'];
				
				$success = TRUE;
				$error_message = 'Sending failed, please try again.';
				if (valid_email($admin_email)) {
					$user_name 		= $this->input->post('name');
					$user_email 	= $this->input->post('email');
					$user_message 	= "Question from: $user_name ($user_email) \n\n" . $this->input->post('message');
					
					// TODO: SEND MESSAGE!
					// TODO: should we implement simple captcha?
					$title = 'Question from ' . $user_name;
					$success = $this->_send_email(
						$admin_email,
						$user_message,
						$title,
						'WGchat Question Mailer',
						$user_email,
						$user_name
					);
					
				} else {
					$error_message = 'Sorry, email can\'t be sent due to invalid administrator email.';
					$success = FALSE;
				}
				
				if ($success) {
					$this->output
						->set_content_type('application/json')
						->set_output(json_encode(array(
							'status'	=> 200,
							'message'	=> 'OK'
						)));		
				} else {
					$this->output
					->set_status_header(500)
					->set_content_type('application/json')
					->set_output(json_encode(array(
						'status'	=> 500,
						'message'	=> $error_message
					)));	
				}
			}
		} else {
			$this->output
				->set_status_header(400)
				->set_content_type('application/json')
				->set_output(json_encode(array(
					'status'	=> 400,
					'message'	=> 'Not allowed, please check API Key!'
				)));
		}
	}
	
	public function transcript()
	{
		$api_key 	= $this->input->post('api_key');
		$parent_url	= $this->input->post('parent_url');
		
		if ($this->input->is_ajax_request() && $api_key != FALSE) {
			$autorize_result = $this->_authorize_widget($api_key, 'http://' . $parent_url);
			
			if (count($autorize_result) == 0) {
				return;
			}
			
			$this->load->helper('email');
		
			$email 			= $this->input->post('email');
			$transcript 	= $this->input->post('transcript');
			$workgroupDesc	= $this->input->post('workgroup_description');
			
			// TODO: send email to user
			$error_message = "[ERROR] Can not send transcript to $email";
			$success = true;
			if (valid_email($email)) {
				// TODO: SEND MESSAGE!
				// TODO: should we implement simple captcha?
				$title = 'Chat Transcript with ' . $workgroupDesc;
				$success = $this->_send_email(
					$email,
					$transcript,
					$title,
					'WGchat Transcript Mailer'
				);
			} else {
				$error_message = 'Sorry, email can\'t be sent due to invalid email.';
				$success = FALSE;
			}
	
			if ($success) {
				$this->output
					->set_content_type('application/json')
					->set_output(json_encode(array(
						'status'	=> 200,
						'message'	=> "Transcript sent to $email"
					)));
			} else {
				$this->output
					->set_status_header('400')
					->set_content_type('application/json')
					->set_output(json_encode(array(
						'status'	=> 400,
						'message'	=> $error_message
					)));
			}		
		} else {
			
			$this->output
				->set_status_header(400)
				->set_content_type('application/json')
				->set_output(json_encode(array(
					'status'	=> 400,
					'message'	=> 'Not allowed, please check API Key!'
				)));
		}
	}
	
	public function iframe($api_key = FALSE)
	{
		if ($this->agent->is_referral() && $api_key != FALSE) {
			$data = $this->_prepare_frame($api_key, $this->agent->referrer());
			
			if ($data == FALSE) {
				return;
			}
			
			$data['isWidgetized']	= TRUE;
			
			// Taking the widget size from config file
			$size_template = $this->config->item('wgchat_widget_size');
			$size_template = $size_template[$data['widgetSize']];
			$data['container_size']	= 'width: ' . $size_template['container_size']['width'] . 'px; height: ' . $size_template['container_size']['height'] . 'px;';
			$data['footer_style']	= 'width: ' . $size_template['container_size']['width'] . 'px;';
			
			$data['chat_area']		= $this->load->view('chatwidget/chat_area', $data, TRUE);
			$data['content']		= $this->load->view('chatwidget/iframe', $data, TRUE);
			
			$this->load->view('chatwidget/main', $data);	
		}
	}
	
	public function blank($api_key = FALSE)
	{
		$referrer = $this->agent->referrer();
		$browser = $this->input->user_agent();
		$is_ie = strripos($browser, '; MSIE');
		
		if ($is_ie != FALSE && $referrer == FALSE) {
			$referrer = $this->input->get('ref');
		}
		
		if ($api_key != FALSE && $referrer != FALSE) {
			$data = $this->_prepare_frame($api_key, $referrer);
			$data['parentUrl'] = $referrer;
			
			if ($data == FALSE) {
				return;
			}
			
			$data['isWidgetized']	= FALSE;
			$data['chat_area']		= $this->load->view('chatwidget/chat_area', $data, TRUE);
			$data['content']		= $this->load->view('chatwidget/blank', $data, TRUE);
			
			$this->load->view('chatwidget/main', $data);	
		}
	}
	
	function _apikey_check($str, $page_id) 
	{
		$widget		= $this->Widgets_model->verify_facebook_widget($str, $page_id);
		
		if ($widget == FALSE) {
			$this->form_validation->set_message('_apikey_check', 'Please enter valid API key');
		
			return FALSE;
		} else {
			return TRUE;
		}	
	}
	
	public function add_facebookwidget()
	{
		$this->load->config('facebook');
		$app_id = $this->config->item('appId');	
		$redirect_uri = str_replace("http:", "https:", site_url('chatwidget/facebookwidget'));
		
		$dialog_url = 'https://www.facebook.com/dialog/pagetab?app_id=' 
			. $app_id . '&redirect_uri=' . $redirect_uri;
		
		redirect($dialog_url);
	}
	
	/**
	 * Verify supplied API Key with specified database record
	 */
	public function verify_facebookwidget($page_id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules(array(
			array(
				'field'	=> 'api_key',
				'label'	=> 'API key',
				'rules'	=> "trim|required|callback__apikey_check[" . $page_id . "]|xss_clean"
			)
		));
		
		if ($this->form_validation->run($this) == FALSE) {
			$this->load->view('verify_facebookwidget', array(
				'page_id'	=> $page_id
			));
		} else {
			$this->load->view('verify_success');
		}
	}
	
	/**
	 * This function is created to facilitate cookie path uniqueness!
	 */
	public function facebookframe($page_id)
	{
		$referrer	= $this->session->flashdata('referrer');
		$api_key	= $this->session->flashdata('api_key');
		
		if ($api_key != FALSE && $referrer != FALSE) {
			$data = $this->_prepare_frame($api_key, $referrer);
			
			if ($data == FALSE) {
				$this->load->view('something_error');
				return;
			}
			
			$data['user_json']		= json_encode($this->session->flashdata('user'));
			$data['app_id']			= $page_id;
			$data['isWidgetized']	= FALSE;
			$data['footer_style']	= 'width: 518px; bottom: 1px;';
			$data['chat_area']		= $this->load->view('chatwidget/chat_area', $data, TRUE);
			$data['content']		= $this->load->view('chatwidget/facebook_frame', $data, TRUE);
			
			$this->load->view('chatwidget/main', $data);	
		}
	}
	
	public function facebookwidget()
	{
		$tabs_added = $this->input->get('tabs_added');
		if ($tabs_added !== FALSE) {
			// There is tabs_added parameter, show page tab addition success
			
			$this->load->config('facebook');
			$this->load->view('widget_add_success', array(
				'pages'	=> $tabs_added,
				'app_id' => $this->config->item('appId')
			));
			return;
		}
		
		$this->load->library('facebook');
		
		$signed_request = $this->facebook->getSignedRequest();
		
		$app_id		= $this->facebook->getAppId();
		$page_id 	= $signed_request['page']['id'];
		$is_admin	= $signed_request['page']['admin'];
		$is_liked	= $signed_request['page']['liked'];
		
		$user 		 	 = null;
		$is_authorized	 = isset($signed_request['user_id']);
		$raw_key 		 = $this->Widgets_model->get_facebook_widget_key($page_id);
		$widget_verified = (count($raw_key) > 0);
		
		if ( ! $widget_verified) {
			if ($is_admin) {
				redirect(site_url('chatwidget/verify_facebookwidget/' . $page_id));
				return;
			} else {
				$this->load->view('something_error');
				return;
			}
		}
		
		if ($is_authorized && $widget_verified) {
			try {
				$uid = $this->facebook->getUser();
				$me = $this->facebook->api('/me');
				
				$user = array(
					'id'		=> $me['id'],
					'username'	=> $me['name'],
					'email'		=> $me['email'],
					'location'	=> "https://www.facebook.com/$page_id"
				);
				
			} catch (FacebookApiException $e) {
				$this->load->view('something_error');
				return;
			}
			
			// Slightly modificated version of function blank()
			$referrer	= $this->agent->referrer();
			$api_key	= $raw_key['api_key'];
			
			$this->session->set_flashdata(array(
				'referrer'	=> $referrer,
				'api_key'	=> $api_key,
				'app_id'	=> $app_id,
				'user'		=> $user
			));
			
			if ($api_key != FALSE && $referrer != FALSE) {
				redirect(site_url('chatwidget/facebookframe/' . $page_id . '?utm_source=' . $user['location'] . '&utm_medium=facebook-widget&utm_campaign=WGchat%20Live%20Chat'));
			}
		} else {
			// User unauthorized, redirect to oauth authorization
			$callback_url = "https://www.facebook.com/pages/null/$page_id/?sk=app_$app_id";	
			
			$this->load->view('facebook_auth', array(
				'app_id'		=> $app_id,
				'callback_url'	=> $callback_url
			));
		}
	}
}