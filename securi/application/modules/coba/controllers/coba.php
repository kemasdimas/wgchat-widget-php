<?php if ( ! defined('BASEPATH') || ENVIRONMENT == 'd') exit('No direct script access allowed');

class Coba extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Widgets_model');
		
		$result = $this->Widgets_model->generate_api_key('kemas');
		
		var_dump($result);
		
		$result = $this->Widgets_model->list_widgets(1);
		
		var_dump($result);
		
		// Get default widget color from config
		$this->load->config('wgchat');
		$widget_default_color = $this->config->item('wgchat_default_color_scheme');
		echo $widget_default_color;
		
		var_dump($this->config->item('wgchat-form-trigger'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */