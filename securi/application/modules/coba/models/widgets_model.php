<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widgets_model extends CI_Model {
	
	private $table_name = 'widgets';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function generate_api_key($workgroup_name, $channel_id = NULL)
	{
		$datemilis 	= date('U');
		if ($channel_id == NULL) {
			$channel_id	= 'widget' . $datemilis . $workgroup_name;
		}
		$api_key	= md5($channel_id);
		
		return array(
			'api_key'		=> $api_key,
			'channel_id'	=> $channel_id
		);
	}
	
	public function list_widgets($id_user)
	{
		$query = $this->db
			->select('id, description, url, channel_name, is_facebook')
			->where('id_user', $id_user)
			->get($this->table_name);
			
		return $query->result_array();
	}
	
	public function update_widget_trigger($id, $href = '', $timeout = -1)
	{
		return $this->db
			->where('id', $id)
			->update($this->table_name, array(
				'trigger_timeout'	=> $timeout,
				'trigger_href'		=> $href
			));
	}
	
	public function update_facebook_widget($id, $description, $online_text, 
		$offline_text, $offline_email, $header_text, $channel_name, $color_scheme)
	{
		$result = $this->update_widget(
			$id, 
			'Facebook Widget', 
			$description, 
			'facebook.com', 
			$online_text, 
			$offline_text, 
			$offline_email, 
			$header_text, 
			$channel_name, 
			$color_scheme);
		
		return $result;
	}
	
	public function update_widget($id, $title, $description, $domain, $online_text, 
		$offline_text, $offline_email, $header_text, $channel_name, $color_scheme)
	{
		$result = $this->db
			->update($this->table_name, array(
				'widget_title' => $title,
				'description' => $description,
				'url' => $domain,
				'online_text' => $online_text,
				'offline_text' => $offline_text,
				'offline_email' => $offline_email,
				'header_text' => $header_text,
				'channel_name' => $channel_name,
				'color_scheme' => $color_scheme,
			));
		
		return $result;
	}
	
	public function create_facebook_widget($channel_id, $id_user, $description, $online_text, 
		$offline_text, $offline_email, $header_text, $channel_name, $color_scheme)
	{
		return $this->create_widget(
			$channel_id, 
			$id_user, 
			'Facebook Widget', 
			$description, 
			'facebook.com', 
			$online_text, 
			$offline_text, 
			$offline_email, 
			$header_text, 
			$channel_name, 
			$color_scheme);
	}
	
	public function create_widget($channel_id, $id_user, $title, $description, $domain, $online_text, 
		$offline_text, $offline_email, $header_text, $channel_name, $color_scheme, $is_facebook = 0)
	{
		$api_key = $this->generate_api_key($channel_id);
		
		$result = $this->db->insert($this->table_name, array(
			'id_user' => $id_user,
			'widget_title' => $title,
			'description' => $description,
			'api_key' => $api_key['api_key'],
			'is_facebook' => $is_facebook,
			'url' => $domain,
			'online_text' => $online_text,
			'offline_text' => $offline_text,
			'offline_email' => $offline_email,
			'header_text' => $header_text,
			'channel_id' => $channel_id,
			'channel_name' => $channel_name,
			'color_scheme' => $color_scheme,
		));
		
		if ($result == TRUE) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
}
