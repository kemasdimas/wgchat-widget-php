<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dynamic extends MX_Controller {
	
	private $general_key = 'general-script-id';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->config('wgchat');
		$this->load->config('wgchat_env');
		
		$cache_time = $this->config->item('wgchat_dynamic_script_cache');
		$cache_adapter = 'file';
		
		if ($cache_time == 0) {
			$cache_adapter = 'dummy';
		}
		
		$this->load->driver('cache', array('adapter' => $cache_adapter, 'backup' => 'file'));
	}
	
	private function _authorize_widget($api_key, $complete_url, $force_preview = FALSE)
	{
		$host_url 		= parse_url($complete_url, PHP_URL_HOST);
		$preview_url	= parse_url(base_url(), PHP_URL_HOST);
		
		$preview = $host_url == $preview_url;
		
		$this->load->model('chatwidget/Widgets_model');
		$widget_data = $this->Widgets_model->get_widget($api_key, $host_url, $preview || $force_preview);
		
		if (count($widget_data) == 0) {
			$this->output->set_status_header('401');
			$this->output->set_output('Not Allowed! Make sure you use exact API Key for this website. Your url now is: ' . $complete_url);
		}
		
		return $widget_data;	
	}
	
	private function _prepare_general_script()
	{
		$saved_view	= $this->cache->get($this->general_key);
		
		if ( ! $saved_view) {
			
			// TODO: Don't forget to change this to 'livechatscript' before uploading!			
			$view_buffer = $this->load->view('livechatscript', array(), true);
			
			$this->cache->save($this->general_key, $view_buffer, $this->config->item('wgchat_dynamic_general_script_cache'));
			$saved_view = $view_buffer;
		}
		
		return $saved_view;
	}
	
	private function _prepare_script($api_key, $is_widgetized, $widget) 
	{
		$fullFrameSrc 	= $this->config->item('wgchat_widget_iframe_src');
		$popupSrc		= $this->config->item('wgchat_widget_popup_src');
		
		$color_scheme = $this->config->item('wgchat_default_color_scheme');
		if ($widget['color_scheme'] != NULL && strlen(trim($widget['color_scheme'])) > 0) {
			$color_scheme = $widget['color_scheme'];
		}
		
		$trigger_timeout = -1;
		if ($widget['trigger_timeout'] != NULL && strlen(trim($widget['trigger_timeout'])) > 0) {
			$trigger_timeout = $widget['trigger_timeout'];
		}
		
		$trigger_href = '';
		if ($widget['trigger_href'] != NULL && strlen(trim($widget['trigger_href'])) > 0) {
			$trigger_href = $widget['trigger_href'];
		}
		
		$chatpop_url = '';
		if ($widget['chatpop_url'] != NULL && strlen(trim($widget['chatpop_url'])) > 0) {
			$chatpop_url = $widget['chatpop_url'];
		}

		$size_template = $this->config->item('wgchat_widget_size');
		$size_template = $size_template[$widget['widget_size']];
		$mini_widget = $size_template['mini_widget_style'];
		
		$data = array(
			'api_key'			=> $api_key,
			'full_frame_src'	=> $fullFrameSrc,
			'popup_src'			=> $popupSrc,
			'is_widgetized'		=> $is_widgetized,
			'color_scheme'		=> $color_scheme,
			'trigger_timeout'	=> $trigger_timeout,
			'trigger_href'		=> $trigger_href,
			'caller_img'		=> $chatpop_url,
			'mini_widget'		=> $mini_widget
		);
		
		return $this->load->view('livechatscript_user', $data, true);
	}
	
	/**
	 * Dynamically create javascript and authorize API key vs referrer
	 */
	public function livechat_script($api_key = FALSE, $is_widgetized = FALSE)
	{
		if ($api_key == FALSE || $is_widgetized == FALSE) {
			$this->output->set_status_header('400');
			$this->output->set_output('Bad request, please enter api_key and is_widgetized param!');
			
			return;
		}
		
		$widget = $this->_authorize_widget($api_key, $this->agent->referrer());
		if (count($widget) == 0) {
			$this->output->set_status_header('401');
			$this->output->set_output('Not Allowed! Make sure you use exact API Key for this website. Your url now is: ' . $this->agent->referrer());
			
			return;
		}
		
		$cache_id 	= $api_key . '-' . $is_widgetized;
		$saved_view	= $this->cache->get($cache_id);
		
		if ( ! $saved_view) {			
			$view_buffer = $this->_prepare_script($api_key, $is_widgetized, $widget);
			
			$this->cache->save($cache_id, $view_buffer, $this->config->item('wgchat_dynamic_script_cache'));
			$saved_view = $view_buffer;
		} 
		
		$this->output->set_header("Content-type: text/javascript");
		$this->output->set_output($this->_prepare_general_script() . $saved_view);
	}
	/*
	public function preview_script($api_key = FALSE, $is_widgetized = FALSE)
	{
		if ($api_key == FALSE || $is_widgetized == FALSE) {
			die('Not allowed');
		}
		
		$widget = $this->_authorize_widget($api_key, $this->agent->referrer(), TRUE);
		if (count($widget) == 0) {
			die('Not allowed');
		}
		
		$this->cache->delete($api_key . '-true');
		$this->cache->delete($api_key . '-false');
		
		$this->output->set_header("Content-type: text/javascript");
		$this->output->set_output($this->_prepare_general_script() . $this->_prepare_script($api_key, $is_widgetized, $widget));
	}
	
	
	public function cache_benchmark()
	{
		$this->benchmark->mark('code_start');
		$this->preview_script('d2lkZ2V0MTM0MTgzOTI5NmtlbWFz', 'true');
		$this->benchmark->mark('code_end');
		
		echo 'TIME: ' . $this->benchmark->elapsed_time('code_start', 'code_end');
	}
	
	public function cache_clean()
	{
		$this->cache->clean();
	}
	
	public function cache_info()
	{
		var_dump($this->cache->cache_info());
	}
	/*
	public function php_info()
	{
		phpinfo();
	}
	// USE THIS TO TEST CACHE */
}