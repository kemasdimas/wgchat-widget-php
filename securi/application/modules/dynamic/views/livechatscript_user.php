LiveWGchat.fullFrameSrc	= '<?php echo $full_frame_src . $api_key; ?>';
LiveWGchat.popupSrc 	= '<?php echo $popup_src . $api_key; ?>';
LiveWGchat.widgetized	= <?php echo $is_widgetized; ?>;
LiveWGchat.colorScheme 	= <?php echo $color_scheme; ?>;
LiveWGchat.timeoutVal 	= <?php echo $trigger_timeout; ?>;
LiveWGchat.hrefVal	 	= '<?php echo $trigger_href; ?>';
LiveWGchat.callerSrc	= '<?php echo $caller_img; ?>';
LiveWGchat.widgetSize	= <?php echo $mini_widget; ?>;

LiveWGchat.init();