(function(){var initializing=false,fnTest=/xyz/.test(function(){xyz;})?/\b_super\b/:/.*/;this.Class=function(){};Class.extend=function(prop){var _super=this.prototype;initializing=true;var prototype=new this();initializing=false;for(var name in prop){prototype[name]=typeof prop[name]=="function"&&typeof _super[name]=="function"&&fnTest.test(prop[name])?(function(name,fn){return function(){var tmp=this._super;this._super=_super[name];var ret=fn.apply(this,arguments);this._super=tmp;return ret;};})(name,prop[name]):prop[name];}function Class(){if(!initializing&&this.init)this.init.apply(this,arguments);}Class.prototype=prototype;Class.prototype.constructor=Class;Class.extend=arguments.callee;return Class;};})();(function(window){'use strict';var Porthole={trace:function(s){if(window['console']!==undefined){window.console.log('Porthole: '+s);}},error:function(s){if(window['console']!==undefined){window.console.error('Porthole: '+s);}}};Porthole.WindowProxy=function(){};Porthole.WindowProxy.prototype={post:function(data,targetOrigin){},addEventListener:function(f){},removeEventListener:function(f){}};Porthole.WindowProxyBase=Class.extend({init:function(targetWindowName){if(targetWindowName===undefined){targetWindowName='';}this.targetWindowName=targetWindowName;this.origin=window.location.protocol+'//'+window.location.host;this.eventListeners=[];},getTargetWindowName:function(){return this.targetWindowName;},getOrigin:function(){return this.origin;},getTargetWindow:function(){return Porthole.WindowProxy.getTargetWindow(this.targetWindowName);},post:function(data,targetOrigin){if(targetOrigin===undefined){targetOrigin='*';}this.dispatchMessage({'data':data,'sourceOrigin':this.getOrigin(),'targetOrigin':targetOrigin,'sourceWindowName':window.name,'targetWindowName':this.getTargetWindowName()});},addEventListener:function(f){this.eventListeners.push(f);return f;},removeEventListener:function(f){var index;try{index=this.eventListeners.indexOf(f);this.eventListeners.splice(index,1);}catch(e){this.eventListeners=[];}},dispatchEvent:function(event){var i;for(i=0;i<this.eventListeners.length;i++){try{this.eventListeners[i](event);}catch(e){}}}});Porthole.WindowProxyLegacy=Porthole.WindowProxyBase.extend({init:function(proxyIFrameUrl,targetWindowName){this._super(targetWindowName);if(proxyIFrameUrl!==null){this.proxyIFrameName=this.targetWindowName+'ProxyIFrame';this.proxyIFrameLocation=proxyIFrameUrl;this.proxyIFrameElement=this.createIFrameProxy();}else{this.proxyIFrameElement=null;throw new Error("proxyIFrameUrl can't be null");}},createIFrameProxy:function(){var iframe=document.createElement('iframe');iframe.setAttribute('id',this.proxyIFrameName);iframe.setAttribute('name',this.proxyIFrameName);iframe.setAttribute('src',this.proxyIFrameLocation);iframe.setAttribute('frameBorder','1');iframe.setAttribute('scrolling','auto');iframe.setAttribute('width',30);iframe.setAttribute('height',30);iframe.setAttribute('style','position: absolute; left: -100px; top:0px;');if(iframe.style.setAttribute){iframe.style.setAttribute('cssText','position: absolute; left: -100px; top:0px;');}document.body.appendChild(iframe);return iframe;},dispatchMessage:function(message){var encode=window.encodeURIComponent;if(this.proxyIFrameElement){var src=this.proxyIFrameLocation+'#'+encode(Porthole.WindowProxy.serialize(message));this.proxyIFrameElement.setAttribute('src',src);this.proxyIFrameElement.height=this.proxyIFrameElement.height>50?50:100;}}});Porthole.WindowProxyHTML5=Porthole.WindowProxyBase.extend({init:function(proxyIFrameUrl,targetWindowName){this._super(targetWindowName);this.eventListenerCallback=null;},dispatchMessage:function(message){this.getTargetWindow().postMessage(Porthole.WindowProxy.serialize(message),message.targetOrigin);},addEventListener:function(f){if(this.eventListeners.length===0){var self=this;this.eventListenerCallback=function(event){self.eventListener(self,event);};if(window.addEventListener){window.addEventListener('message',this.eventListenerCallback,false);}else if(window.attachEvent){window.attachEvent("onmessage",this.eventListenerCallback);}}return this._super(f);},removeEventListener:function(f){this._super(f);if(this.eventListeners.length===0){if(window.addEventListener){window.removeEventListener('message',this.eventListenerCallback);}else if(window.attachEvent){window.detachEvent("onmessage",this.eventListenerCallback);}this.eventListenerCallback=null;}},eventListener:function(self,nativeEvent){var data=Porthole.WindowProxy.unserialize(nativeEvent.data);if(data&&(self.targetWindowName==''||data.sourceWindowName==self.targetWindowName)){self.dispatchEvent(new Porthole.MessageEvent(data.data,nativeEvent.origin,self));}}});if(typeof window.postMessage==='undefined'){Porthole.trace('Using legacy browser support');Porthole.WindowProxy=Porthole.WindowProxyLegacy.extend({});}else{Porthole.trace('Using built-in browser support');Porthole.WindowProxy=Porthole.WindowProxyHTML5.extend({});}Porthole.WindowProxy.serialize=function(obj){if(typeof JSON==='undefined'){throw new Error('Porthole serialization depends on JSON!');}return JSON.stringify(obj);};Porthole.WindowProxy.unserialize=function(text){if(typeof JSON==='undefined'){throw new Error('Porthole unserialization dependens on JSON!');}try{var json=JSON.parse(text);}catch(e){return false;}return json;};Porthole.WindowProxy.getTargetWindow=function(targetWindowName){if(targetWindowName===''){return top;}else if(targetWindowName==='top'||targetWindowName==='parent'){return window[targetWindowName];}return parent.frames[targetWindowName];};Porthole.MessageEvent=function MessageEvent(data,origin,source){this.data=data;this.origin=origin;this.source=source;};Porthole.WindowProxyDispatcher={forwardMessageEvent:function(e){var message,decode=window.decodeURIComponent,targetWindow,windowProxy;if(document.location.hash.length>0){message=Porthole.WindowProxy.unserialize(decode(document.location.hash.substr(1)));targetWindow=Porthole.WindowProxy.getTargetWindow(message.targetWindowName);windowProxy=Porthole.WindowProxyDispatcher.findWindowProxyObjectInWindow(targetWindow,message.sourceWindowName);if(windowProxy){if(windowProxy.origin===message.targetOrigin||message.targetOrigin==='*'){windowProxy.dispatchEvent(new Porthole.MessageEvent(message.data,message.sourceOrigin,windowProxy));}else{Porthole.error('Target origin '+windowProxy.origin+' does not match desired target of '+message.targetOrigin);}}else{Porthole.error('Could not find window proxy object on the target window');}}},findWindowProxyObjectInWindow:function(w,sourceWindowName){var i;if(w.RuntimeObject){w=w.RuntimeObject();}if(w){for(i in w){if(w.hasOwnProperty(i)){try{if(w[i]!==null&&typeof w[i]==='object'&&w[i]instanceof w.window.Porthole.WindowProxy&&w[i].getTargetWindowName()===sourceWindowName){return w[i];}}catch(e){}}}}return null;},start:function(){if(window.addEventListener){window.addEventListener('resize',Porthole.WindowProxyDispatcher.forwardMessageEvent,false);}else if(window.attachEvent&&window.postMessage!=='undefined'){window.attachEvent('onresize',Porthole.WindowProxyDispatcher.forwardMessageEvent);}else if(document.body.attachEvent){window.attachEvent('onresize',Porthole.WindowProxyDispatcher.forwardMessageEvent);}else{Porthole.error('Cannot attach resize event');}}};if(typeof window.exports!=='undefined'){window.exports.Porthole=Porthole;}else{window.Porthole=Porthole;}})(this);
var LiveWGchat = {
	isInteracted: false,
	timeoutVal: -1,
	hrefVal: null,
	innerIframe: null,
	callerSrc: null,
	analyticsCampaign: '?utm_source=' + location.href + '&utm_medium=website-widget&utm_campaign=WGchat Live Chat',
	
	positionStyle: function() {
		return 'position: fixed;' + 
			'bottom: 0;' + 
			'display: none;' + 
			'z-index:2147483000;' +
			'overflow: hidden;';
	},
	
	iframeDefaultStyle: function() {
		return 'position: relative;' + 
			'top: 0px; left: 0;' + 
			'width: 100%;' + 
			'border: 0;padding: 0;margin: 0;' + 
			'float: none;background: none';
	},
	
	popupChat: function() {
		window.open(location.protocol + LiveWGchat.popupSrc + LiveWGchat.analyticsCampaign.replace('website-widget', 'popup-window') + '&ref=' + window.location, 
			'newWindow', 'resizable=0,location=0,width=400,height=450');
	},
	
	openChat: function() {
		if (!LiveWGchat.widgetized) {
			LiveWGchat.popupChat();
		} else {
			var full = document.getElementById('livewgchat-full-container');
			var minim = document.getElementById('livewgchat-compact-container');
			
			minim.style.display = 'none';
			full.style.display = 'block';
			
			windowProxy.post({ type: 'chatOpen', data: { } });
		}
		
		LiveWGchat.isInteracted = true;
	},
	
	hideChat: function() {
		if (!LiveWGchat.widgetized) {
			return;
		}
		
		var full = document.getElementById('livewgchat-full-container');
		var minim = document.getElementById('livewgchat-compact-container');
		
		minim.style.display = 'block';
		full.style.display = 'none';
	},
	
	hideCaller: function() {
		if (LiveWGchat.innerIframe) {
			var caller = LiveWGchat.innerIframe.contentWindow.document.getElementById('wgchat-caller-container');
			if (caller) {
				var miniWidget = document.getElementById('livewgchat-compact-container');
				miniWidget.style.height = '33px';
				LiveWGchat.innerIframe.height = 33;
				
				caller.style.cssText = caller.style.cssText.replace('display: block;', 'display: none;');
			
				windowProxy.post({ type: 'userInteraction', data: {} });	
			}
		}
	},
	
	showCaller: function() {
		if (LiveWGchat.innerIframe) {
			var caller = LiveWGchat.innerIframe.contentWindow.document.getElementById('wgchat-caller-container');
			if (caller) {
				var miniWidget = document.getElementById('livewgchat-compact-container');
				miniWidget.style.height = '250px';
				LiveWGchat.innerIframe.height = 250;
				
				caller.style.cssText = caller.style.cssText.replace('display: none;', 'display: block;');	
			}
		}
	},
	
	generateMinimized: function() {
		var widget = document.createElement('div');
		widget.id = 'livewgchat-compact-container';
		
		widget.style.cssText = LiveWGchat.positionStyle() + 'right: 15px;';
		
		widget.style.width = '250px';
		widget.style.height = LiveWGchat.widgetSize.mini.iframeHeight + 'px';
		
		var innerIframe = document.createElement('iframe');
		innerIframe.id = "wgchat-compact-frame";
		// innerIframe.src = 'about:blank';
		innerIframe.frameBorder = 0;
		innerIframe.marginHeight = 0;
		innerIframe.marginWidth = 0;
		innerIframe.scrolling = 'no';
		innerIframe.height = LiveWGchat.widgetSize.mini.iframeHeight;
		innerIframe.allowtransparency = 'true';
		innerIframe.style.cssText = LiveWGchat.iframeDefaultStyle();
		
		innerIframe.onload = function() {
			var contentDiv = document.createElement('div');
			contentDiv.onclick = LiveWGchat.openChat;
			contentDiv.id = 'wgchat-content-container';
			contentDiv.style.cssText = 'cursor:pointer;display:block; position:absolute;' + 
				'margin: 0px;' + 
				'padding: ' + LiveWGchat.widgetSize.mini.padding + ';' + 
				'right:0;bottom:0;left:0;' + 
				'background:' + LiveWGchat.colorScheme.header_background + ';' + 
				'border: 1px solid #222;' +
				'box-shadow: 1px 4px 3px #888;' + 
				'-moz-border-radius:5px; -moz-border-radius-bottomleft:0; -moz-border-radius-bottomright:0;' + 
				'-webkit-border-radius:5px; -webkit-border-bottom-left-radius:0; -webkit-border-bottom-right-radius:0;' + 
				'border-radius:5px; border-bottom-left-radius:0; border-bottom-right-radius:0;';
			
			var aShowAll = document.createElement('a');
			aShowAll.href = 'javascript:void(null)';
			aShowAll.style.cssText = 'display:block;position:relative;' + 
				'cursor:pointer;outline:0;font-size:' + LiveWGchat.widgetSize.mini.fontSize + ';font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;' + 
				'color:' + LiveWGchat.colorScheme.header_text_color + ';text-shadow:rgb(0, 0, 0) 1px 1px 0px;text-decoration:none;font-weight:normal;';
			
			aShowAll.innerHTML = '<span id="wgchat-widget-header-button" style="display: block; text-align: center;">Online - Chat Sekarang</span>';
			
			contentDiv.appendChild(aShowAll);
			innerIframe.contentWindow.document.body.appendChild(contentDiv);
			
			if (LiveWGchat.callerSrc && LiveWGchat.callerSrc.length > 0) {
				var callerDiv = document.createElement('div');
				callerDiv.id = 'wgchat-caller-container';
				callerDiv.style.cssText = 'display: none; position: absolute; bottom: 0; cursor: pointer; width: 250px; overflow: hidden;';
				
				var callerHider = document.createElement('a');
				callerHider.onclick = LiveWGchat.hideCaller;
				callerHider.href = 'javascript:void(null)';
				callerHider.style.cssText = 'float: right; text-decoration: none; color: #999; font-family: Helvetica, Arial; cursor: pointer;';
				callerHider.innerHTML = 'x';
				callerDiv.appendChild(callerHider);
				
				var imgCaller	= document.createElement('img');
				imgCaller.src	= LiveWGchat.callerSrc;
				imgCaller.alt	= '-';
				imgCaller.onclick = LiveWGchat.openChat;
				callerDiv.appendChild(imgCaller);
				
				innerIframe.contentWindow.document.body.appendChild(callerDiv);
			}
			
			LiveWGchat.innerIframe = innerIframe;
		}
		widget.appendChild(innerIframe);
		
		return widget;
	},
	
	generateMaximized: function() {
		var widget = document.createElement('div');
		widget.id = 'livewgchat-full-container';
		
		widget.style.cssText = LiveWGchat.positionStyle() + 'right: 5px;';
		
		widget.style.width = LiveWGchat.widgetSize.maxi.iframeWidth + 'px';
		widget.style.height = LiveWGchat.widgetSize.maxi.iframeHeight + 'px';
		widget.style.display = 'none';
		
		var innerIframe = document.createElement('iframe');
		innerIframe.name = "wgchat-frame";
		innerIframe.id = "wgchat-frame";
		innerIframe.src = location.protocol + LiveWGchat.fullFrameSrc + LiveWGchat.analyticsCampaign;
		innerIframe.frameBorder = 0;
		innerIframe.scrolling = 'no';
		innerIframe.allowtransparency = 'true';
		innerIframe.style.cssText = LiveWGchat.iframeDefaultStyle();
		innerIframe.style.height = '100%';
		
		windowProxy = new Porthole.WindowProxy(LiveWGchat.fullFrameSrc, innerIframe.name);
		
	    // Register an event handler to receive messages;
	    windowProxy.addEventListener(function(event) {
	    	if (window.console !== undefined) {
	    		//console.log(event);
	    	}
	    	
	    	if (event.data.type === 'frameReady') {
	    		var headerText	= event.data.data.headerText;
	    		var isAvailable = event.data.data.isAvailable;
	    		var isNewComer	= event.data.data.isNewComer;
	    		
	    		document.getElementById('wgchat-compact-frame').
	    			contentWindow.document.getElementById('wgchat-widget-header-button').
	    			innerHTML = headerText;
	    		
	    		LiveWGchat.hideChat();
	    		
	    		// customizable trigger
	    		if (isAvailable) {
	    			if (isNewComer) {
	    				LiveWGchat.showCaller();
	    				
	    				// Trigger only called for new comer
	    				LiveWGchat.triggerTimer(LiveWGchat.timeoutVal);
						LiveWGchat.triggerHref(LiveWGchat.hrefVal);	
	    			}
	    		}
	    	} else if (event.data.type === 'updateHeader') {
	    		document.getElementById('wgchat-compact-frame').
	    			contentWindow.document.getElementById('wgchat-widget-header-button').
	    			innerHTML = event.data.data.headerText;
	    	} else if (event.data.type === 'hideChat') {
	    		LiveWGchat.hideChat();
	    	} else if (event.data.type === 'openChat') {
	    		LiveWGchat.openChat();
	    	} else if (event.data.type === 'getLocation') {
	    		windowProxy.post({ type: 'getLocation', data: { location: window.location.href } });
	    	}
	    });
		widget.appendChild(innerIframe);
		
		return widget;
	},
	
	/**
	 * @param timeout timeout before chat is automatically popped.
	 */
	triggerTimer: function(timeoutVal) {
		var timeout = parseInt(timeoutVal);
		
		if (timeout > -1 && ! LiveWGchat.isInteracted) {
			setTimeout(function() {
				LiveWGchat.openChat();
			}, timeout * 1000);	
		}
	},
	
	triggerHref: function(hrefValue) {
		if (hrefValue.length > 0) {
			var isMatch = hrefValue.indexOf(window.location.href) !== -1;
			
			if (isMatch && ! LiveWGchat.isInteracted) {
				LiveWGchat.openChat();
			}	
		}
	},
	
	init: function() {
		if (LiveWGchat.widgetized) {
			Porthole.WindowProxyDispatcher.start();
			
			var minWidget = LiveWGchat.generateMinimized();	
			var maxWidget = LiveWGchat.generateMaximized();
			
			document.getElementsByTagName('body')[0].appendChild(maxWidget);	
			document.getElementsByTagName('body')[0].appendChild(minWidget);
		}
	}
};