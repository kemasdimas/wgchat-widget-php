<!DOCTYPE html>
<html lang="en">
<head>
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<![endif]-->
	<meta charset="utf-8">
	<title>WGchat.com Widget Preview</title>

	<style type="text/css">
	
	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>
<h1>Widget Preview</h1>
<button onclick="if (LiveWGchat.openChat) { LiveWGchat.openChat(); }">Chat Now!</button><br/>
<pre>onclick="if (LiveWGchat.openChat) { LiveWGchat.openChat(); }"</pre><br/>
<button onclick="if (LiveWGchat.popupChat) { LiveWGchat.popupChat(); }">[POPUP] Chat Now!</button><br/>
<pre>onclick="if (LiveWGchat.popupChat) { LiveWGchat.popupChat(); }"</pre>
<p>To disable widget in the bottom right corner of the page, modify widget: 'true' to widget: 'false' in your widget script.</p>

<!-- WGCHAT WIDGET CODE START -->
<script type="text/javascript">
	(function () {
		var WGchatInitGlobal = {
			apiKey: '<?php echo $api_key; ?>',
			widget:	'true' // 'true' to enable widget and 'false' to disable widget
		};
		
	    var e = document.createElement('script');
	    e.src = '<?php echo base_url(); ?>dynamic/livechat_script/' + WGchatInitGlobal.apiKey + '/' + WGchatInitGlobal.widget;
	    e.async = true;
	    document.getElementsByTagName('body')[0].appendChild(e);
	}());
</script>
<!-- WGCHAT WIDGET CODE END -->
</body>
</html>