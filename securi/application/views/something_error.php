<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--[if IE ]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<![endif]-->
		
		<link type="text/css" rel="stylesheet" href="<?php echo site_url('min/g=widget_css'); ?>" />
	</head>
	<body>
		<div class="hero-unit">
			<h1>Oops.. Something went wrong</h1>
			<p>Sorry, we have some issues while loading this page, please try again later :)</p>
		</div>
	</body>
</html>