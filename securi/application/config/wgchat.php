<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['wgchat_widget_size'] = array(
	array(
		'container_size' => array(
			'width' 	=> 336,
			'height'	=> 380
		),
		'mini_widget_style' => "{ mini: { iframeHeight: 35, padding: '5px 10px 8px', fontSize: '16px' }, maxi: { iframeHeight: 380, iframeWidth: 341 } }"
	),
	array(
		'container_size' => array(
			'width' 	=> 280,
			'height'	=> 270
		),
		'mini_widget_style' => "{ mini: { iframeHeight: 29, padding: '3px 10px 5px', fontSize: '13px' }, maxi: { iframeHeight: 270, iframeWidth: 285 } }"
	)
);
$config['wgchat_default_color_scheme'] 	= '{"header_background":"#663D7F","header_text_color":"#ffffff"}';
$config['wgchat_global_ga_account']		= 'UA-33586123-1';

$config['wgchat-form-trigger'] = array(
	array(
		'field'	 => 'trigger_timeout',
		'label'	 => 'Trigger Timer',
		'rules' => 'trim|integer|greater_than[0]|xss_clean'
	),
	array(
		'field'	 => 'trigger_href',
		'label'	 => 'Trigger URL',
		'rules' => 'trim|xss_clean'
	)
);

$config['wgchat-form-widget'] = array(
	array(
		'field'	 => 'widget_title',
		'label'	 => 'Widget Title',
		'rules' => 'trim|required|max_length[50]|xss_clean'
	),
	array(
		'field'	 => 'description',
		'label'	 => 'Description',
		'rules' => 'trim|max_length[140]|xss_clean'
	),
	array(
		'field'	 => 'domain',
		'label'	 => 'Domain',
		'rules' => 'trim|required|max_length[350]|xss_clean'
	),
	array(
		'field'	 => 'online_text',
		'label'	 => 'Online Text',
		'rules' => 'trim|required|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'offline_text',
		'label'	 => 'Offline Text',
		'rules' => 'trim|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'header_text',
		'label'	 => 'Header Text',
		'rules' => 'trim|required|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'channel_name',
		'label'	 => 'Channel Name',
		'rules' => 'trim|required|max_length[140]|xss_clean'
	),
	array(
		'field'	 => 'color_scheme',
		'label'	 => 'Color Scheme',
		'rules' => 'trim|xss_clean'
	),
	array(
		'field'	 => 'offline_email',
		'label'	 => 'Offline Email',
		'rules' => 'trim|required|max_length[90]|valid_email|xss_clean'
	)
);

$config['wgchat-form-facebook-widget'] = array(
	array(
		'field'	 => 'description',
		'label'	 => 'Description',
		'rules' => 'trim|max_length[140]|xss_clean'
	),
	array(
		'field'	 => 'online_text',
		'label'	 => 'Online Text',
		'rules' => 'trim|required|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'offline_text',
		'label'	 => 'Offline Text',
		'rules' => 'trim|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'header_text',
		'label'	 => 'Header Text',
		'rules' => 'trim|required|max_length[300]|xss_clean'
	),
	array(
		'field'	 => 'channel_name',
		'label'	 => 'Channel Name',
		'rules' => 'trim|required|max_length[140]|xss_clean'
	),
	array(
		'field'	 => 'color_scheme',
		'label'	 => 'Color Scheme',
		'rules' => 'trim|xss_clean'
	),
	array(
		'field'	 => 'offline_email',
		'label'	 => 'Offline Email',
		'rules' => 'trim|required|max_length[90]|valid_email|xss_clean'
	)
);
