<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['wgchat_dynamic_script_cache']			= 5 * 60;
$config['wgchat_dynamic_general_script_cache']	= 1 * 12 * 30 * 24 * 60 * 60;

$config['wgchat_widget_iframe_src'] 	= '//widget.manage.wgchat.com/chatwidget/iframe/';
$config['wgchat_widget_popup_src'] 		= '//widget.manage.wgchat.com/chatwidget/blank/';