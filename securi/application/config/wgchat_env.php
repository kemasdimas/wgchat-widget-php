<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['wgchat_dynamic_script_cache']			= 0;
$config['wgchat_dynamic_general_script_cache']	= 0;

// TODO: change IP to localhost
$config['wgchat_widget_iframe_src'] 	= '//localhost/chatboxci/chatwidget/iframe/';
$config['wgchat_widget_popup_src'] 		= '//localhost/chatboxci/chatwidget/blank/';
