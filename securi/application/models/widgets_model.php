<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widgets_model extends CI_Model {
	
	private $table_name = 'widgets';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_widget($api_key)
	{
		$query = $this->db
			->select('api_key, is_facebook')
			->limit(1)
			->where('api_key', $api_key)
			->get($this->table_name);
	
		return $query->row_array();
	}
}
