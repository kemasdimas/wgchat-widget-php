<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preview extends CI_Controller {
	
	public function widget()
	{
		$preview = $this->input->get('preview');
		$api_key = $this->input->get('api_key');
		
		$this->load->model('Widgets_model');
		$result = $this->Widgets_model->get_widget($api_key);
		
		if (count($result) === 0) {
			die('Invalid API Key');	
		}
		
		if ($preview == true) {
			// Enabling Cache to delete existing cache!
			$this->load->config('wgchat');
			$this->load->config('wgchat_env');
				
			$cache_time = $this->config->item('wgchat_dynamic_script_cache');
			$cache_adapter = 'file';
			
			if ($cache_time == 0) {
				$cache_adapter = 'dummy';
			}
			
			$this->load->driver('cache', array('adapter' => $cache_adapter, 'backup' => 'file'));
			
			$this->cache->delete($api_key . '-true');
			$this->cache->delete($api_key . '-false');
			
			if ($result['is_facebook'] == 1) {
				$this->load->view('facebook_preview_page', array(
					'api_key'	=> $api_key
				));	
			} else {
				$this->load->view('preview_page', array(
					'api_key'	=> $api_key
				));	
			}
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */