if (WGchat_GLOBAL().isWidget) {
	var windowProxy = new Porthole.WindowProxy('http://' + WGchat_GLOBAL().parentUrl);
	
	// Register an event handler to receive messages;
	windowProxy.addEventListener(function(event) {
		if (event.data.type === 'getLocation') { 
			WGchat_API.location = event.data.data.location;
		}
	});
}

var WorkgroupForm = {
    name: 'workgroup-form',
    ns: 'http://jivesoftware.com/protocol/workgroup',
    
    build: function(from, to) {
        return $iq({
            from: from,
            to: to,
            type: 'get',
            id: WGchat_Backend.connection.getUniqueId()
        }).c(WorkgroupForm.name, {
            xmlns: WorkgroupForm.ns
        });
    }
};

var JoinQueue = {
    name: 'join-queue',
    ns: 'http://jabber.org/protocol/workgroup',
    
    build: function(from, to, userId, data) {
        var iq = $iq({
            from: from,
            to: to,
            type: 'set',
            id: WGchat_Backend.connection.getUniqueId()
        });
        
        var joinq = iq.c(JoinQueue.name, {xmlns: JoinQueue.ns})
            .c('queue-notifications').up()
            .c('user', {xmlns: JoinQueue.ns, id: userId}).up();
        
        var fields = joinq.c('x', {xmlns: 'jabber:x:data', type: 'submit'});
        $.each(data, function(elemidx, elem) {
            fields.c('field', {'var': elem.id, type: 'text-single'}).c('value').t(elem.value).up().up();
        });
        
        return iq;
    }
};

var WGchat_Backend = {
	boshUrl:  WGchat_GLOBAL().boshUrl,//'http://kemass-macbook-air.local:8080/http-bind/',
    xmppDomain:  WGchat_GLOBAL().xmppDomain,
    resource: 'WGwidget',
    connection: null,
    isWorkgroupAvailable: false,
    isAttachmentFailed: true,
    
    openConnection: function(handler) {
    	if (WGchat_Backend.connection === null) {
        	
       	}
       	
       	WGchat_Backend.connection = new Strophe.Connection(WGchat_Backend.boshUrl);
        WGchat_Backend.connection.connect(WGchat_Backend.xmppDomain + '/' + WGchat_Backend.resource, '', handler);
        WGchat_Backend.connection.resume();
        
        // WGchat_Backend.connection.addHandler(WGchat_Backend.onPacketLog);
    },
    
    attachConnection: function(handler) {
        var jid = $.cookie('wgchat-jid', { expires: 360 });
        var sid = $.cookie('wgchat-sid', { expires: 360 });
        var rid = $.cookie('wgchat-rid', { expires: 360 });
        
        WGchat_Backend.connection = new Strophe.Connection(WGchat_Backend.boshUrl);
        WGchat_Backend.connection.attach(jid, sid, parseInt(rid) + 1, handler);
        WGchat_Backend.connection.resume();
        
        // WGchat_Backend.connection.addHandler(WGchat_Backend.onPacketLog);
    },
    
    closeConnection: function(reason) {
    	if (reason === undefined) {
    		reason = 'disconnect';
    	}
    	
        if (WGchat_Backend.connection !== null) {
            WGchat_Backend.connection.disconnect(reason);
            WGchat_Backend.connection.reset();
            
            WGchat_Backend.connection = null;
            
            // // console.log('disconnected: ' + WGchat_Backend.connection);
        }
    },
    
    isCookieAvailable: function() {
        var room = $.cookie('wgchat-roomjid');
        var jid = $.cookie('wgchat-jid');
        var sid = $.cookie('wgchat-sid');
        var rid = $.cookie('wgchat-rid');
        
        if (room !== null && jid !== null && sid !== null && rid !== null) {
            return true;
        } else {
            WGchat_Backend.resetCookie();
            return false;
        }
    },
    
    resetCookie: function() {
        $.cookie('wgchat-roomjid', null);
        $.cookie('wgchat-jid', null);
        $.cookie('wgchat-sid', null);
        $.cookie('wgchat-rid', null);
    },
    
    onPacketLog: function(packet) {
        // console.log('LOG Packet: ' + (new Date()));
        // console.log(packet);
        
        return true;
    },
    
    handlePingIq: function(packet) {
    	var $packet = $(packet);
    	var from 	= $packet.attr('from');
    	var to		= $packet.attr('to');
    	var id		= $packet.attr('id');
    	
    	var result = $iq({
            from: to,
            to: from,
            type: 'result',
            id: id
        });
        
        WGchat_Backend.send(result);
    	
    	return true;
    },
    
    handleRidUpdate: function(packet) {
        $.cookie('wgchat-rid', WGchat_Backend.connection.rid, { expires: 360 });
        
        return true;
    },
    
    send: function(packet) {
    	WGchat_Backend.connection.resume();
    	WGchat_Backend.connection.send(packet);
    },
    
    getDataCookie: function() {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			return JSON.parse(existingData);
    		}
    	}
    	
    	return null;
    },
    
    /**
     * @param data object with key value pair
     */
    storeDataCookie: function(data) {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			existingData = JSON.parse(existingData);
    			existingData = WGchat_Backend.mergeOptions(existingData, data);
    		} else {
    			existingData = data;
    		}
    		
    		// console.log(existingData);
    		
    		$.cookie('wgchat-form-data', JSON.stringify(existingData), { expires: 30 });
    	}
    },

	clearDataCookie: function() {
		var storedData = WGchat_Backend.getDataCookie();
		
		if (storedData !== null && storedData.userID !== undefined) {
			var userId = storedData.userID;
		}
		
		$.cookie('wgchat-form-data', null);
		
		// Restore the userID
		WGchat_Backend.storeDataCookie({
			userID: userId
		});
	},
    
    /**
     * http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
	 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
	 * @param obj1
	 * @param obj2
	 * @returns obj3 a new object based on obj1 and obj2
	 */
	mergeOptions: function(obj1,obj2){
	    var obj3 = {};
	    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	    return obj3;
	}
};

var WGchat_API = {
	workgroup: WGchat_GLOBAL().workgroup,
	roomOccupants: 0,
	isActive: false,
	
	roomJoinedHandler: null,
	roomjid: null,
	nickname: null,
	agentNodes: {},
	historyBuffer: '',
	
	resetData: function() {
		WGchat_API.isActive = false;
		WGchat_API.roomJoinedHandler = null;
		WGchat_API.roomjid = null;
		WGchat_API.nickname = null;
		WGchat_API.roomOccupants = 0;
		WGchat_API.agentNodes = {};
	},
	
	onlineWorkgroupCheck: function() {
	   	if (WGchat_Backend.isCookieAvailable()) {
        	WGchat_API.setLoaderVisible(true, 'Resuming chat...');
       	}
       	
       	$('#btn-start-chat').hide();
		
        WGchat_Backend.openConnection(WGchat_API.workgroupCheckConnectionHandler);
    },
	
	/*
	 * HANDLER REGION
	 */
	workgroupCheckConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTED) {
            // try to send presence to workgruop
            WGchat_Backend.connection.addHandler(WGchat_API.handleWorkgroupPresence, 
            	'http://jivesoftware.com/protocol/workgroup', 'presence');
            
            var pres = $pres({
            	id: WGchat_Backend.connection.getUniqueId(),
            	from: WGchat_Backend.connection.jid, 
            	to: WGchat_API.workgroup
            });
            
            WGchat_Backend.send(pres);
        } else if (status === Strophe.Status.AUTHENTICATING) {
            // console.log('Authenticated!');
        } else if (status === Strophe.Status.CONNFAIL) {
            // console.log('Workgroup Check connection failed: ' + msg)
        } else if (status === Strophe.Status.AUTHFAIL) {
            // console.log('Workgroup Check authentication failed: ' + msg)
        } else if (status === Strophe.Status.DISCONNECTED) {
            // console.log('Workgroup check closed: ' + msg);
        }
    },
    
    handleWorkgroupPresence: function(presence) {
        WGchat_Backend.closeConnection();
    	
    	WGchat_API.setLoaderVisible(false);
    	
        var isAvailable = $(presence).attr('type') !== 'unavailable';
        WGchat_Backend.isWorkgroupAvailable = isAvailable;
        
        if (isAvailable) {
        	$('#offline-form').remove();
        	
            if (WGchat_Backend.isCookieAvailable()) {
            	$('#btn-start-chat').hide();
				
				WGchat_API.resumeChat();
            } else {
            	$('#btn-start-chat').show();
				$('#btn-start-chat').on('click', WGchat_API.startChat);
            }
        } else {
        	var headerText 	= $('#header .text');
        	var offlineForm = $('#offline-form');
        	
        	headerText.text('[Offline] ' + headerText.text());
        	offlineForm.removeClass('hidden');
        	WGchat_Backend.resetCookie();
        	
        	offlineForm.find('[type=submit]').click(function(evt) {
        		evt.preventDefault();
        		evt.stopPropagation();
        		
        		offlineForm.find('.alert').remove();
        		
        		var name 	= offlineForm.find('#offline-username').val();
        		var email 	= offlineForm.find('#offline-email').val();
        		var message	= offlineForm.find('#offline-message').val();
        		
        		WGchat_API.sendOfflineMessage(offlineForm, $(evt.currentTarget), name, email, message);
        	});
        }
        
        if (windowProxy) {
        	// console.log('READY FRAME!' + 'http://' + WGchat_GLOBAL().parentUrl);
        	
	        windowProxy.post({ type: 'frameReady', data: {
				headerText: WGchat_GLOBAL().onlineText
			} });
			
			$('#header').on('click', function() {
		    	windowProxy.post({ type: 'hideChat', data: {} });
			});
		}
    },
    
    mainConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTING) {
            // console.log('Connecting...');
        } else if (status === Strophe.Status.AUTHENTICATING) {
            // console.log('Authenticated!');
        } else if (status === Strophe.Status.CONNECTED) {
        	// console.log('Connected!');
            // Try to initiate chat with support
            var domain = Strophe.getDomainFromJid(WGchat_Backend.connection.jid);
            var wgJid = WGchat_API.workgroup;
            var formReq = WorkgroupForm.build(WGchat_Backend.connection.jid, wgJid);
            var id = $(formReq.tree()).attr('id');
            
            WGchat_Backend.connection.addHandler(WGchat_API.handleWorkgroupForm, WorkgroupForm.ns, 'iq', null, id);
            WGchat_Backend.send(formReq);
            
            // Add iq ping handler
            WGchat_Backend.connection.addHandler(WGchat_Backend.handlePingIq, 'urn:xmpp:ping', 'iq');
        } else if (status === Strophe.Status.ATTACHED) {
            // console.log('Connection attached.');
            
            // WGchat_Backend.connection.addHandler(WGchat_Backend.onPacketLog);
            
            // Group presence handler
        	WGchat_Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
            WGchat_API.roomjid = $.cookie('wgchat-roomjid');
			WGchat_Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            	'presence', null, null, WGchat_API.roomjid);

            WGchat_Backend.send($pres({to: WGchat_API.roomjid, type: 'unavailable'}).
                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
            WGchat_Backend.send($pres({to: WGchat_API.roomjid}).
                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
                
            // Add iq ping handler
            WGchat_Backend.connection.addHandler(WGchat_Backend.handlePingIq, 'urn:xmpp:ping', 'iq');
        } else if (status === Strophe.Status.CONNFAIL) {
            // console.log('Connection failed. REASON: ' + msg);
            // WGchat_Backend.resetCookie();
        } else if (status === Strophe.Status.DISCONNECTED) {
            // console.log('Connection terminated. REASON: ' + msg);
            
            if (WGchat_Backend.isAttachmentFailed === true) {
            	WGchat_API.setLoaderVisible(false);
	    		WGchat_API.setChatVisible(true);
	    		WGchat_API.closeChat('Session closed. <span><a id="btn-restart-chat" href="javascript:void(null)">Start new chat?</a></span>');
            }
        }
    },
    
    handleWorkgroupForm: function(packet) {
    	
        var fields = $(packet).find('field');
        var varFields = [];
        fields.each(function(index, elem) {
            var $elem = $(elem);
            
            if ($elem.attr('type') !== 'hidden') {
                var name = $elem.attr('var');
                var label = $elem.attr('label');
                var type = $elem.attr('type');
                var required = $(elem).find('required').length > 0 || name === 'username';
                var values = [];
                
                if ($elem.attr('type') === 'list-multi') {
                    // $('input[name=Jenis_Kelamin]:checked').val()
                    $elem.find('option').each(function(idx, elm) {
                    	var $elm = $(elm);
                    	
                    	values.push($elm.find('value').text());
                    });
                }
                
                varFields.push({
                    id: name,
                    label: label,
                    type: type,
                    values: values,
                    required: required
                });
            }
        });
        
        WGchat_API.buildForm(varFields);
    },
    
    handleInvitation: function(packet) {
    	var room = $(packet).attr('from');
        var nickname = 'Customer';
        
        // Send online presence
        WGchat_Backend.send($pres().c('status').t('online').up().c('priority').t('1'));
        
        WGchat_API.acceptInvitation(room, nickname);
    },
    
    handleInvitationError: function(packet) {
        var jid = $(packet).attr('from');
        var room = Strophe.getBareJidFromJid(jid);
        var nickname = Strophe.getResourceFromJid(jid);
        
        WGchat_API.acceptInvitation(room, nickname + '-' + WGchat_Backend.connection.getUniqueId());
    },
    
    handleRoomJoined: function(packet) {
    	// set the attachmentfialed to false
    	WGchat_Backend.isAttachmentFailed = false;
    	
        WGchat_API.roomjid = $(packet).attr('from');
        WGchat_API.nickname = Strophe.getResourceFromJid($(packet).attr('from'));
        
        // Write to cookie
        $.cookie('wgchat-roomjid', WGchat_API.roomjid);
        $.cookie('wgchat-jid', WGchat_Backend.connection.jid);
        $.cookie('wgchat-sid', WGchat_Backend.connection.sid);
        $.cookie('wgchat-rid', WGchat_Backend.connection.rid);
        
        // Must add the cookie RID updater
        WGchat_Backend.connection.addHandler(WGchat_Backend.handleRidUpdate);
        
        // Live message handler
        WGchat_Backend.connection.addHandler(WGchat_API.handleLiveMessage, null, 'message', 'groupchat');
        
        WGchat_API.setLoaderVisible(false);
        WGchat_API.setChatVisible(true);
        
        // Add event handler to chat input box
        $('#txt-chat-input').on('keypress', WGchat_API.handleChatboxInput);
        
        if (windowProxy) {
        	windowProxy.post({ type: 'openChat', data: { }});
        }
	},
    
    handleGroupPresence: function(packet) {
        if ($(packet).attr('type') === 'unavailable') {
            WGchat_API.roomOccupants--;
            
            if (WGchat_API.roomOccupants < 0) {
                WGchat_API.roomOccupants = 0;
            }
        } else {
            WGchat_API.roomOccupants++;
            
            if (WGchat_API.roomOccupants > 1) {
                WGchat_API.isActive = true;
            }
            
            // Request other user vcard!
            if ($(packet).attr('from').indexOf("Customer") === -1) {
            	/**
					<iq from='stpeter@jabber.org/roundabout'
					    id='v3'
					    to='jer@jabber.org'
					    type='get'>
					  <vCard xmlns='vcard-temp'/>
					</iq>
            	 */
            	
            	var iqFrom	= WGchat_Backend.connection.jid;
            	var iqTo 	= Strophe.getResourceFromJid($(packet).attr('from')) + '@' + WGchat_Backend.xmppDomain;
            	var id 		= WGchat_Backend.connection.getUniqueId();
            	var iq		= $iq({ from: iqFrom, to: iqTo, id: id, type: 'get' }).c('vCard', { xmlns: 'vcard-temp' });
            	
            	// // console.error(iq.toString());
            	
            	// WGchat_Backend.send(iq); 
            }
        }
        
        if (WGchat_API.isActive && WGchat_API.roomOccupants === 1) {
            WGchat_Backend.closeConnection();
        	
            WGchat_API.roomOccupants = 0;
            
            WGchat_API.closeChat();
            
            return false;
        }
        
        return true;
    },
   
	handleLiveMessage: function(packet) {
		var jidNick = Strophe.getResourceFromJid($(packet).attr('from'));
		var $packet = $(packet);
		
        // Check whether the conference is closed
        if (jidNick === null) {
        	WGchat_API.closeChat();
            WGchat_Backend.closeConnection();
            
            return false;
        } else if ($packet.find('x').size() === 0) {
        	return true;
        }
    
        if ($(packet).find('body').size() > 0) {
            // message received
            var body = $(packet).find('body').text();
            var isMe = (WGchat_API.nickname === jidNick) ? 0 : 1;
            
            if (isMe === 0) {
            	jidNick = 'Me';
            }
            
            // Check timestamp in body
            var timeStamp = null;
            if ($(packet).find('delay').length > 0 
            	&& $(packet).find('delay').attr('stamp') !== null) {
            	
            	var rawStamp = $(packet).find('delay').attr('stamp').split('T')[1].split(':');
            	var gmtHours = -(new Date()).getTimezoneOffset()/60;
            	
            	timeStamp = (parseInt(rawStamp[0], 10) + gmtHours) + ':' + rawStamp[1];
            } else {
            	// Play sound for new message
            	// soundManager.onready(function() {
					// soundManager.play('incomingchat');
				// });
            }
            
            // append message
            setTimeout(function() {
            	WGchat_API.addChatMessage(isMe, jidNick, body, timeStamp);	
            }, 250);
        }
        
        return true;
	},
    
    handleChatboxInput: function(evt) {
    	var $txtArea = $(evt.target);
    	
    	if (evt.keyCode == 13 && evt.shiftKey == 0) {
    		var message = $txtArea.val();
    		message = message.replace(/^\s+|\s+$/g,"");
    		
    		$txtArea.val('');
    		$txtArea.focus();
    		
    		if ($.trim(message.toString()) !== '') {
    			var jid = Strophe.getBareJidFromJid(WGchat_API.roomjid);
                
                WGchat_Backend.send($msg({
                    to: jid,
                    'type': 'groupchat'
                }).c('body').t(message));
                
                WGchat_API.addChatMessage(0, 'Me', message);
    		}
    		
    		return false;
    	}
    },
    
    handleQueueStatus: function(packet) {
    	// console.log(packet);
    	
    	if ($(packet).find('depart-queue').length > 0 &&
    		$(packet).find('depart-queue').find('agent-not-found').length > 0) {
    		
    		// Show queue full notification!
    		WGchat_API.setLoaderVisible(false);
    		WGchat_API.setChatVisible(true);
    		WGchat_API.closeChat('Sorry, no operator available. <span><a id="btn-restart-chat" href="javascript:void(null)">Please try again later.</a></span>');
    		
    		return false;
    	}
    	
    	return true;
    },
    
	/**
	 * END OF HANDLER REGION
	 */
	
	acceptInvitation: function(room, nickname) {
        var id = WGchat_Backend.connection.getUniqueId();
    
        // Add error handler
        WGchat_Backend.connection.addHandler(WGchat_API.handleInvitationError, 'http://jabber.org/protocol/muc', 'presence', 'error', null, id);
        
        // Add room joined handler, check whether it is not null
        if (WGchat_API.roomJoinedHandler !== null) {
            // Strophe.deleteHandler(WGchat_API.roomJoinedHandler);
        }
        WGchat_API.roomJoinedHandler = WGchat_Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            'presence', null, null, room + '/' + nickname);
        
        // Group presence handler
        WGchat_Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
        var pres = $pres({to: room + '/' + nickname, id: id}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
        WGchat_Backend.send(pres);
    },
	
	setLoaderVisible: function(visible, loaderText) {
		var defaultText = "Loading...";
		var $loader = $('#loading-indicator');
		
		if (visible) {
			$loader.show();
			
			var textContainer = $('#loading-text');
			textContainer.html('<h3>' + loaderText || defaultText + '</h3>');
		} else {
			$loader.hide();
		}
	},
	
	setChatVisible: function(visible) {
		var $chat = $('#chat-area, #chat-input-area');
		
		if (visible) {
			$chat.show();
			$('#inner-chat-area').empty();
			$('#chat-area').removeClass('full-chat-area');
			$('#txt-chat-input').watermark('Type message here, and press enter to send');
			
			$('#btn-close-chat').off('click');
			$('#btn-close-chat').on('click', WGchat_API.endChat);
			$('#btn-close-chat').show();
			
			WGchat_API.scrollChat(true);
			WGchat_API.resizeChatArea();
		} else {
			$chat.hide();
			
			$('#btn-close-chat').off('click');
			$('#btn-close-chat').hide();
		}
	},
	
	setFormVisible: function(visible) {
		var $form = $('#form-area');
		
		if (visible) {
			$form.show();
		} else {
			$form.hide();
		}
	},
	
	resumeChat: function() {
		WGchat_API.setLoaderVisible(true, 'Resuming chat...');
		
		WGchat_Backend.attachConnection(WGchat_API.mainConnectionHandler);
	},
	
	startChat: function() {
		WGchat_API.scrollChat(true);
		
		WGchat_API.setChatVisible(false);
		WGchat_API.setFormVisible(false);
		
		$('#btn-start-chat').hide();
		WGchat_API.setLoaderVisible(true, 'Starting chat session...');
		
		// reset the data
		WGchat_API.resetData();
		
		WGchat_Backend.openConnection(WGchat_API.mainConnectionHandler);
	},
	
	endChat: function(evt) {
		if (evt !== undefined) {
			evt.stopImmediatePropagation();	
		}
		
		var confAnswer = confirm('Want to close this chat?');
		if (confAnswer) {
			var id = WGchat_Backend.connection.getUniqueId();
        
	        var pres = $pres({to: WGchat_API.roomjid, id: id, type: 'unavailable'}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
	        WGchat_Backend.send(pres);
	        WGchat_Backend.closeConnection();
	        
	        WGchat_API.closeChat();
		}
	},
	
	validateEmail: function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	   
	    return re.test(email);
	}, 
	
	closeChat: function(text) {
		
		if (text === undefined) {
			// if ( ! WGchat_GLOBAL().isWidget) {
		// // check if popup windows is closing
		// $(window).bind('beforeunload', function() {
			// return 'Are you sure to close this chat?';
		// });
	// }
	
			text = 'Chat ended.<br/><br/><span><a class="btn btn-small" id="btn-restart-chat" href="javascript:void(null)"><i class="icon-comment"></i>&nbsp;Chat Again</a>' + 
				'&nbsp;<a class="btn btn-small btn-primary" id="btn-send-transcript" href="javascript:void(null)"><i class="icon-envelope icon-white"></i>&nbsp;Send Transcript</a>'
			
			if ( ! WGchat_GLOBAL().isWidget) {
				// Add close window button if not widget
				text += '&nbsp;<a class="btn btn-small btn-danger" id="btn-close-window" href="javascript:void(null)"><i class="icon-remove icon-white"></i>&nbsp;Close</a>';
			}
			
			text += '</span>';
			
			WGchat_API.addChatNotification(text);
		} else {
			WGchat_API.addChatNotification(text);
		}
		
		$('#chat-input-area').off('keypress');
		$('#chat-input-area').hide();
		
		$('#btn-close-chat').hide();
		
		$('#btn-restart-chat').off('click');
		$('#btn-send-transcript').off('click');
		$('#btn-close-window').off('click');
		
		$('#btn-restart-chat').on('click', WGchat_API.startChat);
		$('#btn-send-transcript').on('click', WGchat_API.sendTranscript);
		$('#btn-close-window').on('click', function() {
			window.close();
		});
		
		WGchat_Backend.resetCookie();
		
		WGchat_API.resizeChatArea(true);
	},
	
	sendOfflineMessage: function(formContainer, button, name, email, message) {
		var postData = {
			name: name,
			email: email,
			message: message,
			'api_key': WGchat_GLOBAL().apiKey,
			'parent_url': WGchat_GLOBAL().parentUrl
		};
		
		var jqxhr = $.post(WGchat_GLOBAL().baseUrl + 'chatwidget/offline_message', postData,			
			function(data) {
				formContainer.find('.control-group input, .control-group textarea').attr('disabled', '');
		
				button.addClass('btn-success disabled');
				button.html('<i class="icon-white icon-ok"></i>&nbsp;Message sent!');
	    	}, 'json')
	    .error(function(data) {
	    	data = $.parseJSON(data.responseText);
	    	
    		formContainer.append(
    			'<div class="alert alert-error">' + 
    				'<button class="close" data-dismiss="alert">x</button>' + data.message + 
    			'</div>');
    		
    		button.button('reset');
	    });	
		
		button.button('loading');
	},
	
	sendTranscript: function(evt) {
		var $button = $(evt.currentTarget);
		if ($button.attr('class').indexOf('disabled') !== -1) {
			return;
		}
		
		$button.button();
		$button.button('loading');
		
		// Check email address in cookie
		var cookieData = WGchat_Backend.getDataCookie();
		var emailAddress = cookieData['email'] || '';
		
		var emailAddress = prompt("Enter your email address: ", emailAddress);
		if (emailAddress === null) {
			return;
		}
		
		if ($.trim(emailAddress.toString()).length > 0 && WGchat_API.validateEmail(emailAddress)) {
			var date = new Date();
			var postData = { 
				email: emailAddress, 
				transcript: WGchat_API.historyBuffer,
				'api_key': WGchat_GLOBAL().apiKey,
				'parent_url': WGchat_GLOBAL().parentUrl
			};
			
			WGchat_API.historyBuffer += '\nDate: ' + date.toString() + '\n=== generated by WGchat.com - Empowering Online Chat';
			
			var jqxhr = $.post(WGchat_GLOBAL().baseUrl + 'chatwidget/transcript', 
				postData,			
				function(data) {
					WGchat_API.addChatNotification(data.message, 'success');
					
					// Remove btn start chat after transcript sending succeed
					var $btnSend = $('#btn-send-transcript');
					$btnSend.remove();
		    	}, 'json')
		    .error(function(data) {
		    	$button.button('reset');
		    	data = $.parseJSON(data.responseText);
		    
		    	WGchat_API.addChatNotification(data.message, 'error');
		    });	
		    
		    // Store email address in cookie
		    WGchat_Backend.storeDataCookie({
		    	'email': emailAddress
		    });
		} else {
			$button.button('reset');
			WGchat_API.addChatNotification("Invalid email address, please retry using valid email address.", 'error');
		}
	},
	
	checkRealName: function(jidNode) {
		var name = WGchat_API.agentNodes[jidNode];
		
		// return (name !== undefined) ? name : jidNode;
		
		return jidNode.split('-')[0];
	},
	
	/**
	 * side = 0 / 1, 0 = me, 1 = other
	 */
	addChatMessage: function(side, from, body, timeStamp) {
		var $chatArea = $('#inner-chat-area');
		
		var $message = $('<div class="chat-message"></div>');
		var me = (side === 0) ? 'me' : '';
		
		var date = new Date();
		
		if (!timeStamp) {
			var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
				(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		}
		
		var bodies = body.split(/\r\n|\r|\n/);
		$message.append(
			'<span class="username ' + me + '">' + WGchat_API.checkRealName(from) + '<span class="right">' + timeStamp + '</span></span>'
		);

		$.each(bodies, function(textidx, text) {
			$message.append(
				'<span class="msg-body">' + text + '</span>'
			);
		});
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
		
		// Save history to buffer
		WGchat_API.historyBuffer += WGchat_API.checkRealName(from) + ' (' + timeStamp + '): ' + body + '\n';
	},
	
	addChatNotification: function(body, extraClass) {
		var $chatArea = $('#inner-chat-area');
		
		var $message = $('<div class="chat-message notification alert alert-info"></div>');
		if (extraClass) {
			$message.addClass(extraClass);
		}
		
		var date = new Date();
		var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
			(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		
		$message.append(
			'<span class="msg-body">(' + timeStamp + ') ' + body + '</span>'
		);
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
	},
	
	scrollChat: function(reset) {
		var $chatArea		= $('#chat-area');
		var $innerChatArea 	= $('#inner-chat-area');
		
		if (reset == true) {
			$chatArea.scrollTop(0);
			//$innerChatArea.css({ marginTop: 0 });	
		} else {
			var chatHeight 	= $chatArea.height() - 11;
			var innerHeight = $innerChatArea.height();
			var delta = innerHeight - chatHeight;
			
			if (delta > 0) {
				$chatArea.scrollTop(delta);	
			}	
		}
	},
	
	buildForm: function(fields) {
		// console.log('FORM');
		
		var $formArea = $('#form-area');
		$formArea.empty();
		
		var $formTag = $('<form action="#"></form>');
		
		// Check value from cookie, if there is a value in cookie, don't show the field
		// auto assign the value to corresponding field.
		var cookieData = WGchat_Backend.getDataCookie();
		
		$.each(fields, function(fieldidx, field) {
			if (cookieData !== null) {
				var aData = cookieData[field.id];
			}
			
			var $div = $('<div class="control-group"></div>');
			var requiredString = '';
			
			if (field.required) {
				requiredString = '&nbsp;<span class="label label-important">required</span>';
			}
			
			$div.append(
				'<label for="' + field.id + '"><h4 style="display: inline;">' + field.label + '</h4>' + requiredString + '</label>'
			);
			
			var storedValue = undefined;
			if (aData !== undefined && aData !== null && $.trim(aData.toString()) !== '') {
				storedValue = cookieData[field.id];
			}
			
			if (field.type === 'text-single') {
				var $container = $div.append(
					'<input type="text" id="' + field.id + '" name="' + field.id + '" />'
				);
				
				$container.find('input').attr('value', storedValue);
			} else {
				$.each(field.values, function(validx, value) {
					var $container = $div.append(
						'<input type="radio" id="' + field.id + '" name="' + field.id + '" value="' + value + '"/>' +
						'&nbsp;' + value + '<br/>'
					);
					
					if (storedValue === value) {
						$container.find('input').attr('checked', true);
					}
				});
			}
			
			$formTag.append($div);
		});
		
		if ($formTag.children().length > 0) {
			$formTag.append(
				'<div class="control-group">' +
					'<input class="btn btn-primary" type="submit" id="form-submit" value="Submit" />' +
				'</div>'
			);
			WGchat_API.setLoaderVisible(false);
			
			$formArea.append($formTag);
			$formArea.show();
			
			// Add form submit handler
			$('#form-submit').off('click');
			$('#form-submit').on('click', function(evt) {
				evt.stopPropagation();
				evt.preventDefault();
				
				WGchat_API.validateForm(fields);
			});	
		} else {
			// Autovalidate the form!
			WGchat_API.sendForm(fields, cookieData['userID']);
		}
	},
	
	validateForm: function(fields) {
		var $scope = $('.control-group');
		var isValid = true;
		
		
		$.each(fields, function(fieldidx, field) {
			if (field.value === undefined) {
				var id = field.id;
				var required = field.required;
				var value = undefined;
				
				var $input = $scope.find('input[name=' + id + ']');
				if (field.type === 'text-single') {
					if ($.trim($input.val().toString()).length > 0) {
	                    value = $.trim($input.val().toString());
	                }
				} else {
					var $input = $scope.find('input[name=' + id + ']:checked');
	                
	                if ($input.length > 0) {
	                    value = $input.val();
	                }
				}
				
				// Check if the field is required
	            $scope.find('label[for=' + id + ']').removeClass('form-error');
	            
	            if (required && value === undefined) {
	                isValid = false;
	                $scope.find('label[for=' + id + ']').addClass('form-error');
	            } else if (!required) {
	                value = '';
	            }
	            
	            // append value to varFields
	            field.value = value;
			}
		});
		
		if (isValid) {
			WGchat_API.sendForm(fields);
		}
	},
	
	sendForm: function(fields, storedUserId) {
		WGchat_API.setFormVisible(false);
		WGchat_API.setLoaderVisible(true, 'Contacting customer services...');
		
        // TODO: userID must be taken from cookie!
        var userId = (storedUserId !== undefined) ? storedUserId : WGchat_GLOBAL().userID;
        var wgJid = WGchat_API.workgroup;
        
        // Append few additional field
        fields.push({
            id: 'userID',
            value: userId
        });
        
        // Store fields to cookie
        $.each(fields, function(fieldidx, field) {
        	var newData = {};
        	newData[field.id] = field.value;
        	
        	WGchat_Backend.storeDataCookie(newData);
        });
        
        var parentLocation = WGchat_API.location;
        if (parentLocation === undefined) {
        	parentLocation = window.location.href;
        }
        fields.push({
            id: 'location',
            value: parentLocation
        });
        
        if ($.trim(WGchat_GLOBAL().channelId.toString()).length > 0) {
        	fields.push({
        		id: 'channel',
        		value: WGchat_GLOBAL().channelId
        	});
        }
        
        var req = JoinQueue.build(WGchat_Backend.connection.jid, wgJid, userId, fields);
        id = $(req.tree()).attr('id');
        
        WGchat_Backend.connection.addHandler(WGchat_API.handleQueueStatus, 'http://jabber.org/protocol/workgroup', 'message');
        WGchat_Backend.connection.addHandler(WGchat_API.handleInvitation, 'jabber:x:conference', 'message');
        WGchat_Backend.send(req);
	},
	
	resizeChatArea: function(hiddenTextArea) {
		var chatArea = $('#chat-area');
		var inputArea = $('#chat-input-area');
		var footerArea = $('#footer')
		var headerArea = $('#header');
		
		var chatHeight = $(window).height() - (hiddenTextArea ? 0 : inputArea.height()) - footerArea.height() - (headerArea.height() + 11);
		chatArea.height(chatHeight - 27);
	},
};

// Setup soundManager
// Below is prone to error!
// soundManager.url = WGchat_GLOBAL().baseUrl + 'assets/js/soundmanager/swf/';
// soundManager.onready(function() {
	// soundManager.createSound({
		// id: 'incomingchat',
		// url: WGchat_GLOBAL().baseUrl + '/assets/sounds/incoming.wav'
	// });
// });

$(document).ready(function() {
	if (WGchat_GLOBAL().isWidget) {
		Porthole.WindowProxyDispatcher.start();
	}
	
	WGchat_API.onlineWorkgroupCheck();
	
	// Get real windows Location
	if (windowProxy) {
		windowProxy.post({ type: 'getLocation', data: { } });
	}
	
	// Handle window resize event
	var resizeTimer;
	$(window).resize(function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(WGchat_API.resizeChatArea, 100);
	});
	
	// Initial resize
	WGchat_API.resizeChatArea();
	
	var header_background	= WGchat_GLOBAL().colorScheme.header_background;
	var header_color		= WGchat_GLOBAL().colorScheme.header_text_color;	
	var borderColor			= (new Color(header_background)).getDarker(30);
	
	// Change color via colorScheme
	$('#header').css({
		backgroundColor: header_background,
		color: header_color,
		borderBottom: '1px solid ' + borderColor.toString()
	});
	
	$('#chat-input-area').css({
		backgroundColor: header_background
	});
});