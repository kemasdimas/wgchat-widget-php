var Porthole=(typeof Porthole=="undefined")||!Porthole?{}:Porthole;Porthole={trace:function(a){try{console.log("Porthole: "+a)}catch(b){}},error:function(a){try{console.error("Porthole: "+a)}catch(b){}}};Porthole.WindowProxy=function(){};Porthole.WindowProxy.prototype={postMessage:function(){},addEventListener:function(a){},removeEventListener:function(a){}};Porthole.WindowProxyLegacy=function(a,b){if(b===undefined){b=""}this.targetWindowName=b;this.eventListeners=[];this.origin=window.location.protocol+"//"+window.location.host;if(a!==null){this.proxyIFrameName=this.targetWindowName+"ProxyIFrame";this.proxyIFrameLocation=a;this.proxyIFrameElement=this.createIFrameProxy()}else{this.proxyIFrameElement=null}};Porthole.WindowProxyLegacy.prototype={getTargetWindowName:function(){return this.targetWindowName},getOrigin:function(){return this.origin},createIFrameProxy:function(){var a=document.createElement("iframe");a.setAttribute("id",this.proxyIFrameName);a.setAttribute("name",this.proxyIFrameName);a.setAttribute("src",this.proxyIFrameLocation);a.setAttribute("frameBorder","1");a.setAttribute("scrolling","auto");a.setAttribute("width",30);a.setAttribute("height",30);a.setAttribute("style","position: absolute; left: -100px; top:0px;");if(a.style.setAttribute){a.style.setAttribute("cssText","position: absolute; left: -100px; top:0px;")}document.body.appendChild(a);return a},postMessage:function(b,a){if(a===undefined){a="*"}if(this.proxyIFrameElement===null){Porthole.error("Can't send message because no proxy url was passed in the constructor")}else{sourceWindowName=window.name;this.proxyIFrameElement.setAttribute("src",this.proxyIFrameLocation+"#"+b+"&sourceOrigin="+escape(this.getOrigin())+"&targetOrigin="+escape(a)+"&sourceWindowName="+sourceWindowName+"&targetWindowName="+this.targetWindowName);this.proxyIFrameElement.height=this.proxyIFrameElement.height>50?50:100}},addEventListener:function(a){this.eventListeners.push(a);return a},removeEventListener:function(b){try{var a=this.eventListeners.indexOf(b);this.eventListeners.splice(a,1)}catch(c){this.eventListeners=[];Porthole.error(c)}},dispatchEvent:function(c){for(var b=0;b<this.eventListeners.length;b++){try{this.eventListeners[b](c)}catch(a){Porthole.error("Exception trying to call back listener: "+a)}}}};Porthole.WindowProxyHTML5=function(a,b){if(b===undefined){b=""}this.targetWindowName=b};Porthole.WindowProxyHTML5.prototype={postMessage:function(b,a){if(a===undefined){a="*"}if(this.targetWindowName===""){targetWindow=top}else{targetWindow=parent.frames[this.targetWindowName]}targetWindow.postMessage(b,a)},addEventListener:function(a){window.addEventListener("message",a,false);return a},removeEventListener:function(a){window.removeEventListener("message",a,false)},dispatchEvent:function(b){var a=document.createEvent("MessageEvent");a.initMessageEvent("message",true,true,b.data,b.origin,1,window,null);window.dispatchEvent(a)}};if(typeof window.postMessage!="function"){Porthole.trace("Using legacy browser support");Porthole.WindowProxy=Porthole.WindowProxyLegacy;Porthole.WindowProxy.prototype=Porthole.WindowProxyLegacy.prototype}else{Porthole.trace("Using built-in browser support");Porthole.WindowProxy=Porthole.WindowProxyHTML5;Porthole.WindowProxy.prototype=Porthole.WindowProxyHTML5.prototype}Porthole.WindowProxy.splitMessageParameters=function(c){if(typeof c=="undefined"||c===null){return null}var e=[];var d=c.split(/&/);for(var b in d){var a=d[b].split("=");if(typeof(a[1])=="undefined"){e[a[0]]=""}else{e[a[0]]=a[1]}}return e};Porthole.MessageEvent=function MessageEvent(c,a,b){this.data=c;this.origin=a;this.source=b};Porthole.WindowProxyDispatcher={forwardMessageEvent:function(c){var b=document.location.hash;if(b.length>0){b=b.substr(1);m=Porthole.WindowProxyDispatcher.parseMessage(b);if(m.targetWindowName===""){targetWindow=top}else{targetWindow=parent.frames[m.targetWindowName]}var a=Porthole.WindowProxyDispatcher.findWindowProxyObjectInWindow(targetWindow,m.sourceWindowName);if(a){if(a.origin==m.targetOrigin||m.targetOrigin=="*"){c=new Porthole.MessageEvent(m.data,m.sourceOrigin,a);a.dispatchEvent(c)}else{Porthole.error("Target origin "+a.origin+" does not match desired target of "+m.targetOrigin)}}else{Porthole.error("Could not find window proxy object on the target window")}}},parseMessage:function(b){if(typeof b=="undefined"||b===null){return null}params=Porthole.WindowProxy.splitMessageParameters(b);var a={targetOrigin:"",sourceOrigin:"",sourceWindowName:"",data:""};a.targetOrigin=unescape(params.targetOrigin);a.sourceOrigin=unescape(params.sourceOrigin);a.sourceWindowName=unescape(params.sourceWindowName);a.targetWindowName=unescape(params.targetWindowName);var c=b.split(/&/);if(c.length>3){c.pop();c.pop();c.pop();c.pop();a.data=c.join("&")}return a},findWindowProxyObjectInWindow:function(a,c){if(a.RuntimeObject){a=a.RuntimeObject()}if(a){for(var b in a){try{if(a[b]!==null&&typeof a[b]=="object"&&a[b] instanceof a.Porthole.WindowProxy&&a[b].getTargetWindowName()==c){return a[b]}}catch(d){}}}return null},start:function(){if(window.addEventListener){window.addEventListener("resize",Porthole.WindowProxyDispatcher.forwardMessageEvent,false)}else{if(document.body.attachEvent){window.attachEvent("onresize",Porthole.WindowProxyDispatcher.forwardMessageEvent)}else{Porthole.error("Can't attach resize event")}}}};
var LiveWGchat = {
	fullFrameSrc: 'http://localhost/chatboxci/chatwidget/iframe',
	
	positionStyle: function() {
		return 'position: absolute;' + 
			'bottom: 0;' + 
			'right: 15px;' +
			'display: none;' + 
			'overflow: hidden;';
	},
	
	iframeDefaultStyle: function() {
		return 'position: relative;' + 
			'top: 0px; left: 0;' + 
			'width: 100%;' + 
			'border: 0;padding: 0;margin: 0;' + 
			'float: none;background: none';
	},
	
	openChat: function() {
		var full = document.getElementById('livewgchat-full-container');
		var minim = document.getElementById('livewgchat-compact-container');
		
		minim.style.display = 'none';
		full.style.display = 'block';
	},
	
	hideChat: function() {
		var full = document.getElementById('livewgchat-full-container');
		var minim = document.getElementById('livewgchat-compact-container');
		
		minim.style.display = 'block';
		full.style.display = 'none';
	},
	
	generateMinimized: function() {
		var widget = document.createElement('div');
		widget.id = 'livewgchat-compact-container';
		
		widget.style.cssText = LiveWGchat.positionStyle();
		
		widget.style.width = '250px';
		widget.style.height = '33px';
		
		var innerIframe = document.createElement('iframe');
		innerIframe.id = "wgchat-compact-frame";
		innerIframe.src = 'javascript:false';
		innerIframe.frameBorder = 0;
		innerIframe.scrolling = 'no';
		innerIframe.allowtransparency = 'true';
		innerIframe.style.cssText = LiveWGchat.iframeDefaultStyle();
		
		innerIframe.onload = function() {
			var extraDiv = document.createElement('div');
			extraDiv.id = 'extra';
			extraDiv.style.cssText = 'display:block; position:absolute;' + 
				'top:0;right:0;bottom:0;left:0;' + 
				'width:100%;height:100%;z-index:5;' + 
				'background:rgba(67, 46, 97, 0.5);' + 
				'-moz-border-radius:10px; -moz-border-radius-bottomleft:0; -moz-border-radius-bottomright:0;' + 
				'-webkit-border-radius:10px; -webkit-border-bottom-left-radius:0; -webkit-border-bottom-right-radius:0;' + 
				'border-radius:10px; border-bottom-left-radius:0; border-bottom-right-radius:0;';
			
			innerIframe.contentWindow.document.body.appendChild(extraDiv);
			
			var contentDiv = document.createElement('div');
			contentDiv.id = 'content-container';
			contentDiv.style.cssText = 'display:block; position:absolute;' + 
				'margin: 7px 7px 0;' + 
				'padding: 4px 8px;' + 
				'top:0;right:0;bottom:0;left:0;' + 
				'z-index:6;' + 
				'background:rgb(102, 61, 127);' + 
				'-moz-border-radius:5px; -moz-border-radius-bottomleft:0; -moz-border-radius-bottomright:0;' + 
				'-webkit-border-radius:5px; -webkit-border-bottom-left-radius:0; -webkit-border-bottom-right-radius:0;' + 
				'border-radius:5px; border-bottom-left-radius:0; border-bottom-right-radius:0;';
			
			var aShowAll = document.createElement('a');
			aShowAll.href = 'javascript:void(null)';
			aShowAll.onclick = LiveWGchat.openChat;
			aShowAll.style.cssText = 'display:block;position:relative;' + 
				'cursor:pointer;outline:0;font-size:14px;font-family:"Lucida Grande","Lucida Sans Unicode",Arial,Verdana,sans-serif;' + 
				'color:rgb(255, 255, 255);text-shadow:rgb(0, 0, 0) 1px 1px 0px;text-decoration:none;font-weight:normal;';
			
			aShowAll.innerHTML = '<span id="compact-widget-header-button">Online - Chat Sekarang</span><span style="float: right;">[+]</span>';
			
			contentDiv.appendChild(aShowAll);
			innerIframe.contentWindow.document.body.appendChild(contentDiv);
		}
		widget.appendChild(innerIframe);
		
		return widget;
	},
	
	generateMaximized: function() {
		var widget = document.createElement('div');
		widget.id = 'livewgchat-full-container';
		
		widget.style.cssText = LiveWGchat.positionStyle();
		
		widget.style.width = '350px';
		widget.style.height = '400px';
		widget.style.display = 'none';
		
		var innerIframe = document.createElement('iframe');
		innerIframe.name = "wgchat-frame";
		innerIframe.id = "wgchat-frame";
		innerIframe.src = LiveWGchat.fullFrameSrc;
		innerIframe.frameBorder = 0;
		innerIframe.scrolling = 'no';
		innerIframe.allowtransparency = 'true';
		innerIframe.style.cssText = LiveWGchat.iframeDefaultStyle();
		innerIframe.style.height = '100%';
		
		windowProxy = new Porthole.WindowProxy(LiveWGchat.fullFrameSrc, innerIframe.name);
		console.log(LiveWGchat.fullFrameSrc);
	    // Register an event handler to receive messages;
	    windowProxy.addEventListener(function(event) {
	    	// console.log(event.data);
	    	
	    	if (event.data.type === 'frameReady') {
	    		document.getElementById('wgchat-compact-frame').
	    			contentWindow.document.getElementById('compact-widget-header-button').
	    			innerHTML = event.data.data.headerText;
	    		
	    		LiveWGchat.hideChat();
	    	} else if (event.data.type === 'hideChat') {
	    		LiveWGchat.hideChat();
	    	} else if (event.data.type === 'openChat') {
	    		LiveWGchat.openChat();
	    	} else if (event.data.type === 'getLocation') {
	    		windowProxy.postMessage({ type: 'getLocation', data: { location: window.location.href } });
	    	}
	    });
		widget.appendChild(innerIframe);
		
		return widget;
	},
	
	init: function() {
		var minWidget = LiveWGchat.generateMinimized();	
		var maxWidget = LiveWGchat.generateMaximized();
		
		document.getElementsByTagName('body')[0].appendChild(maxWidget);	
		document.getElementsByTagName('body')[0].appendChild(minWidget);
		
		// console.log(document.cookie);
	}
};

LiveWGchat.init();
