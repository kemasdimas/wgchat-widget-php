var windowProxy = new Porthole.WindowProxy('http://' + parentUrl);
console.log(parentUrl);

// Register an event handler to receive messages;
windowProxy.addEventListener(function(event) {
	if (event.data.type === 'getLocation') { 
		WGchat_API.location = event.data.data.location;
	}
});

var WorkgroupForm = {
    name: 'workgroup-form',
    ns: 'http://jivesoftware.com/protocol/workgroup',
    
    build: function(from, to) {
        return $iq({
            from: from,
            to: to,
            type: 'get',
            id: Backend.connection.getUniqueId()
        }).c(WorkgroupForm.name, {
            xmlns: WorkgroupForm.ns
        });
    }
};

var JoinQueue = {
    name: 'join-queue',
    ns: 'http://jabber.org/protocol/workgroup',
    
    build: function(from, to, userId, data) {
        var iq = $iq({
            from: from,
            to: to,
            type: 'set',
            id: Backend.connection.getUniqueId()
        });
        
        var joinq = iq.c(JoinQueue.name, {xmlns: JoinQueue.ns})
            .c('queue-notifications').up()
            .c('user', {xmlns: JoinQueue.ns, id: userId}).up();
        
        var fields = joinq.c('x', {xmlns: 'jabber:x:data', type: 'submit'});
        data.forEach(function(elem) {
            fields.c('field', {'var': elem.id, type: 'text-single'}).c('value').t(elem.value).up().up();
        });
        
        return iq;
    }
};

var Backend = {
	boshUrl: 'http://kemass-macbook-air.local:5280/http-bind/',
    xmppDomain: 'xmpp.wgchat.com',
    connection: null,
    isWorkgroupAvailable: false,
    isAttachmentFailed: true,
    
    openConnection: function(handler) {
        Backend.connection = new Strophe.Connection(Backend.boshUrl);
        Backend.connection.connect(Backend.xmppDomain, '', handler);
    },
    
    attachConnection: function(handler) {
        var jid = $.cookie('wgchat-jid');
        var sid = $.cookie('wgchat-sid');
        var rid = $.cookie('wgchat-rid');
        
        console.log('NEXT RID: ' + (parseInt(rid) + 1));
        
        Backend.connection = new Strophe.Connection(Backend.boshUrl);
        Backend.connection.attach(jid, sid, parseInt(rid) + 1, handler);
    },
    
    closeConnection: function(reason) {
        if (Backend.connection !== null) {
            Backend.connection.disconnect(reason);
            
            /*
            console.log('LAST JID: ' + Backend.connection.jid);
            console.log('LAST SID: ' + Backend.connection.sid);
            console.log('LAST RID: ' + Backend.connection.rid);
            */
            Backend.connection = null;
        }
    },
    
    isCookieAvailable: function() {
        var room = $.cookie('wgchat-roomjid');
        var jid = $.cookie('wgchat-jid');
        var sid = $.cookie('wgchat-sid');
        var rid = $.cookie('wgchat-rid');
        
        if (room !== null && jid !== null && sid !== null && rid !== null) {
            return true;
        } else {
            console.log('Cookie ROOM: ' + room);
            console.log('Cookie JID: ' + jid);
            console.log('Cookie SID: ' + rid);
            console.log('Cookie RID: ' + sid);
        
            Backend.resetCookie();
            return false;
        }
    },
    
    resetCookie: function() {
        console.error('COOKIE RESETED!');
    
        $.cookie('wgchat-roomjid', null);
        $.cookie('wgchat-jid', null);
        $.cookie('wgchat-sid', null);
        $.cookie('wgchat-rid', null);
    },
    
    onPacketLog: function(packet) {
        console.log('LOG Packet: ' + (new Date()));
        console.log(packet);
        
        return true;
    },
    
    handleRidUpdate: function(packet) {
        $.cookie('wgchat-rid', Backend.connection.rid);
        
        return true;
    },
    
    send: function(packet) {
    	Backend.connection.send(packet);
    	// Backend.handleRidUpdate(packet);
    },
    
    getDataCookie: function() {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			return JSON.parse(existingData);
    		}
    	}
    	
    	return null;
    },
    
    /**
     * @param data object with key value pair
     */
    storeDataCookie: function(data) {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			existingData = JSON.parse(existingData);
    			existingData = Backend.mergeOptions(existingData, data);
    		} else {
    			existingData = data;
    		}
    		
    		console.log(existingData);
    		
    		$.cookie('wgchat-form-data', JSON.stringify(existingData), { expires: 30 });
    	}
    },

	clearDataCookie: function() {
		var storedData = Backend.getDataCookie();
		
		if (storedData !== null && storedData.userID !== undefined) {
			var userId = storedData.userID;
		}
		
		$.cookie('wgchat-form-data', null);
		
		// Restore the userID
		Backend.storeDataCookie({
			userID: userId
		});
	},
    
    /**
     * http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
	 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
	 * @param obj1
	 * @param obj2
	 * @returns obj3 a new object based on obj1 and obj2
	 */
	mergeOptions: function(obj1,obj2){
	    var obj3 = {};
	    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	    return obj3;
	}
};

var WGchat_API = {
	workgroup: workgroup,
	roomOccupants: 0,
	isActive: false,
	
	roomJoinedHandler: null,
	roomjid: null,
	nickname: null,
	agentNodes: {},
	
	resetData: function() {
		WGchat_API.isActive = false;
		WGchat_API.roomJoinedHandler = null;
		WGchat_API.roomjid = null;
		WGchat_API.nickname = null;
		WGchat_API.roomOccupants = 0;
		WGchat_API.agentNodes = {};
	},
	
	onlineWorkgroupCheck: function() {
        Backend.openConnection(WGchat_API.workgroupCheckConnectionHandler);
    },
	
	/*
	 * HANDLER REGION
	 */
	workgroupCheckConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTED) {
            // try to send presence to workgruop
            Backend.connection.addHandler(WGchat_API.handleWorkgroupPresence, 
            	'http://jivesoftware.com/protocol/workgroup', 'presence');
            
            var pres = $pres({from: Backend.connection.jid, to: WGchat_API.workgroup});
            
            Backend.send(pres);
        } else if (status === Strophe.Status.CONNFAIL) {
            alert('CONFAIL');
        } else if (status === Strophe.Status.DISCONNECTED) {
            console.log('Workgroup check closed: ' + msg);
        }
    },
    
    handleWorkgroupPresence: function(presence) {
        Backend.closeConnection();
    
        var isAvailable = $(presence).attr('type') !== 'unavailable';
        Backend.isWorkgroupAvailable = isAvailable;
        
        if (isAvailable) {
        	windowProxy.postMessage({ type: 'frameReady', data: {
				headerText: onlineText
			} });
        	
            if (Backend.isCookieAvailable()) {
            	$('#btn-start-chat').hide();
				
				WGchat_API.resumeChat();
            } else {
				$('#btn-start-chat').on('click', WGchat_API.startChat);
            }
            
            $('#header').on('click', function() {
				alert('minimize!');
		    	windowProxy.postMessage({ type: 'hideChat', data: {} });
			});
        } else {
        	Backend.resetCookie();
        }
    },
    
    mainConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTING) {
            console.log('Connecting...');
        } else if (status === Strophe.Status.CONNECTED) {
            // Try to initiate chat with support
            var domain = Strophe.getDomainFromJid(Backend.connection.jid);
            var wgJid = WGchat_API.workgroup;
            var formReq = WorkgroupForm.build(Backend.connection.jid, wgJid);
            var id = $(formReq.tree()).attr('id');
            
            Backend.connection.addHandler(WGchat_API.handleWorkgroupForm, WorkgroupForm.ns, 'iq', null, id);
            Backend.send(formReq);
        } else if (status === Strophe.Status.ATTACHED) {
            console.log('Connection attached.');
            
            // Backend.connection.addHandler(Backend.onPacketLog);
            
            // Group presence handler
        	Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
            WGchat_API.roomjid = $.cookie('wgchat-roomjid');
			Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            	'presence', null, null, WGchat_API.roomjid);

            Backend.send($pres({to: WGchat_API.roomjid, type: 'unavailable'}).
                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
            Backend.send($pres({to: WGchat_API.roomjid}).
                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
        } else if (status === Strophe.Status.CONNFAIL) {
            console.log('Connection failed. REASON: ' + msg);
            // Backend.resetCookie();
        } else if (status === Strophe.Status.DISCONNECTED) {
            console.log('Connection terminated. REASON: ' + msg);
            
            if (Backend.isAttachmentFailed === true) {
            	Backend.resetCookie();
            	
            	WGchat_API.setLoaderVisible(false);
	    		WGchat_API.setChatVisible(true);
	    		WGchat_API.closeChat('Session closed. <span><a id="btn-restart-chat" href="javascript:void(null)">Start new chat?</a></span>');
            }
        }
    },
    
    handleWorkgroupForm: function(packet) {
        console.log(packet);
        
        var fields = $(packet).find('field');
        var varFields = [];
        fields.each(function(index, elem) {
            var $elem = $(elem);
            
            if ($elem.attr('type') !== 'hidden') {
                var name = $elem.attr('var');
                var label = $elem.attr('label');
                var type = $elem.attr('type');
                var required = $(elem).find('required').length > 0 || name === 'username';
                var values = [];
                
                if ($elem.attr('type') === 'list-multi') {
                    // $('input[name=Jenis_Kelamin]:checked').val()
                    $elem.find('option').each(function(idx, elm) {
                    	var $elm = $(elm);
                    	
                    	values.push($elm.find('value').text());
                    });
                }
                
                varFields.push({
                    id: name,
                    label: label,
                    type: type,
                    values: values,
                    required: required
                });
            }
        });
        
        WGchat_API.buildForm(varFields);
    },
    
    handleInvitation: function(packet) {
    	var room = $(packet).attr('from');
        var nickname = 'Customer';
        
        // Send online presence
        Backend.send($pres().c('status').t('online').up().c('priority').t('1'));
        
        WGchat_API.acceptInvitation(room, nickname);
    },
    
    handleInvitationError: function(packet) {
        var jid = $(packet).attr('from');
        var room = Strophe.getBareJidFromJid(jid);
        var nickname = Strophe.getResourceFromJid(jid);
        
        WGchat_API.acceptInvitation(room, nickname + '-' + Backend.connection.getUniqueId());
    },
    
    handleRoomJoined: function(packet) {
    	// set the attachmentfialed to false
    	Backend.isAttachmentFailed = false;
    	
        WGchat_API.roomjid = $(packet).attr('from');
        WGchat_API.nickname = Strophe.getResourceFromJid($(packet).attr('from'));
        
        // Write to cookie
        $.cookie('wgchat-roomjid', WGchat_API.roomjid);
        $.cookie('wgchat-jid', Backend.connection.jid);
        $.cookie('wgchat-sid', Backend.connection.sid);
        $.cookie('wgchat-rid', Backend.connection.rid);
        
        // Must add the cookie RID updater
        Backend.connection.addHandler(Backend.handleRidUpdate);
        
        // Live message handler
        Backend.connection.addHandler(WGchat_API.handleLiveMessage, null, 'message', 'groupchat');
        
        WGchat_API.setLoaderVisible(false);
        WGchat_API.setChatVisible(true);
        
        // Add event handler to chat input box
        $('#txt-chat-input').on('keypress', WGchat_API.handleChatboxInput);
        
        windowProxy.postMessage({ type: 'openChat', data: { }});
	},
    
    handleGroupPresence: function(packet) {
        if ($(packet).attr('type') === 'unavailable') {
            WGchat_API.roomOccupants--;
            
            if (WGchat_API.roomOccupants < 0) {
                WGchat_API.roomOccupants = 0;
            }
        } else {
            WGchat_API.roomOccupants++;
            
            if (WGchat_API.roomOccupants > 1) {
                WGchat_API.isActive = true;
            }
            
            // Request other user vcard!
            // if ($(packet).attr('from').indexOf("Customer") === -1) {
            	// /**
					// <iq from='stpeter@jabber.org/roundabout'
					    // id='v3'
					    // to='jer@jabber.org'
					    // type='get'>
					  // <vCard xmlns='vcard-temp'/>
					// </iq>
            	 // */
//             	
            	// var iqFrom	= Backend.connection.jid;
            	// var iqTo 	= Strophe.getResourceFromJid($(packet).attr('from')) + '@' + Backend.xmppDomain;
            	// var id 		= Backend.connection.getUniqueId();
            	// var iq		= $iq({ from: iqFrom, to: iqTo, id: id, type: 'get' }).c('vCard', { xmlns: 'vcard-temp' });
//             	
            	// Backend.connection.addHandler(Backend.onPacketLog, null, 'iq');
            	// Backend.send(iq); 
            // }
        }
        
        if (WGchat_API.isActive && WGchat_API.roomOccupants === 1) {
            Backend.closeConnection();
        	
            WGchat_API.roomOccupants = 0;
            
            WGchat_API.closeChat();
            
            return false;
        }
        
        return true;
    },
   
	handleLiveMessage: function(packet) {
		var jidNick = Strophe.getResourceFromJid($(packet).attr('from'));
		
        // Check whether the conference is closed
        if (jidNick === null) {
        	WGchat_API.closeChat();
            Backend.closeConnection();
            
            return false;
        }
    
        if ($(packet).find('body').size() > 0) {
            // message received
            var body = $(packet).find('body').text();
            var isMe = (WGchat_API.nickname === jidNick) ? 0 : 1;
            
            if (isMe === 0) {
            	jidNick = 'Me';
            }
            
            // Check timestamp in body
            var timeStamp = null;
            if ($(packet).find('delay').length > 0 
            	&& $(packet).find('delay').attr('stamp') !== null) {
            	
            	var rawStamp = $(packet).find('delay').attr('stamp').split('T')[1].split(':');
            	var gmtHours = -(new Date()).getTimezoneOffset()/60;
            	
            	timeStamp = (parseInt(rawStamp[0], 10) + gmtHours) + ':' + rawStamp[1];
            }
            
            // append message
            WGchat_API.addChatMessage(isMe, jidNick, body, timeStamp);
        } else if ($(message).find('composing').size() > 0) {
            // typing notif
            console.log('Agent is typing');
        }
        
        return true;
	},
    
    handleChatboxInput: function(evt) {
    	var $txtArea = $(evt.target);
    	
    	if (evt.keyCode == 13 && evt.shiftKey == 0) {
    		var message = $txtArea.val();
    		message = message.replace(/^\s+|\s+$/g,"");
    		
    		$txtArea.val('');
    		$txtArea.focus();
    		
    		if (message.trim() !== '') {
    			var jid = Strophe.getBareJidFromJid(WGchat_API.roomjid);
                
                Backend.send($msg({
                    to: jid,
                    'type': 'groupchat'
                }).c('body').t(message));
    		}
    		
    		return false;
    	}
    },
    
    handleQueueStatus: function(packet) {
    	console.log(packet);
    	
    	if ($(packet).find('depart-queue').length > 0 &&
    		$(packet).find('depart-queue').find('agent-not-found').length > 0) {
    		
    		// Show queue full notification!
    		WGchat_API.setLoaderVisible(false);
    		WGchat_API.setChatVisible(true);
    		WGchat_API.closeChat('Sorry, no operator available. <span><a id="btn-restart-chat" href="javascript:void(null)">Please try again later.</a></span>');
    		
    		return false;
    	}
    	
    	return true;
    },
    
	/**
	 * END OF HANDLER REGION
	 */
	
	acceptInvitation: function(room, nickname) {
        var id = Backend.connection.getUniqueId();
    
        // Add error handler
        Backend.connection.addHandler(WGchat_API.handleInvitationError, 'http://jabber.org/protocol/muc', 'presence', 'error', null, id);
        
        // Add room joined handler, check whether it is not null
        if (WGchat_API.roomJoinedHandler !== null) {
            // Strophe.deleteHandler(WGchat_API.roomJoinedHandler);
        }
        WGchat_API.roomJoinedHandler = Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            'presence', null, null, room + '/' + nickname);
        
        // Group presence handler
        Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
        var pres = $pres({to: room + '/' + nickname, id: id}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
        Backend.send(pres);
    },
	
	setLoaderVisible: function(visible) {
		var $loader = $('#loading-indicator');
		
		if (visible) {
			$loader.removeClass('hidden');
		} else {
			$loader.addClass('hidden');
		}
	},
	
	setChatVisible: function(visible) {
		var $chat = $('#chat-area, #chat-input-area');
		
		if (visible) {
			$chat.show();
			$('#chat-area').empty();
			$('#chat-area').removeClass('full-chat-area');
			$('#txt-chat-input').watermark('Type message here, and press enter to send');
			
			$('#btn-close-chat').off('click');
			$('#btn-close-chat').on('click', WGchat_API.endChat);
			$('#btn-close-chat').show();
		} else {
			$chat.hide();
			
			$('#btn-close-chat').off('click');
			$('#btn-close-chat').hide();
		}
	},
	
	setFormVisible: function(visible) {
		var $form = $('#form-area');
		
		if (visible) {
			$form.show();
		} else {
			$form.hide();
		}
	},
	
	resumeChat: function() {
		WGchat_API.setLoaderVisible(true);
		
		Backend.attachConnection(WGchat_API.mainConnectionHandler);
	},
	
	startChat: function() {
		WGchat_API.setChatVisible(false);
		WGchat_API.setFormVisible(false);
		
		$('#btn-start-chat').hide();
		WGchat_API.setLoaderVisible(true);
		
		// reset the data
		WGchat_API.resetData();
		
		Backend.openConnection(WGchat_API.mainConnectionHandler);
	},
	
	endChat: function(evt) {
		if (evt !== undefined) {
			evt.stopImmediatePropagation();	
		}
		
		var confAnswer = confirm('Want to close this chat?');
		if (confAnswer) {
			var id = Backend.connection.getUniqueId();
        
	        var pres = $pres({to: WGchat_API.roomjid, id: id, type: 'unavailable'}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
	        Backend.send(pres);
		}
	},
	
	closeChat: function(text) {
		
		if (text === undefined) {
			WGchat_API.addChatNotification('Chat ended. <span><a id="btn-restart-chat" href="javascript:void(null)">Start another chat?</a></span>');
		} else {
			WGchat_API.addChatNotification(text);
		}
		
		$('#chat-area').addClass('full-chat-area');
		
		$('#chat-input-area').off('keypress');
		$('#chat-input-area').hide();
		
		$('#btn-close-chat').hide();
		
		$('#btn-restart-chat').on('click', WGchat_API.startChat);
		
		Backend.resetCookie();
	},
	
	checkRealName: function(jidNode) {
		var name = WGchat_API.agentNodes[jidNode];
		
		// return (name !== undefined) ? name : jidNode;
		
		return jidNode.split('-')[0];
	},
	
	/**
	 * side = 0 / 1, 0 = me, 1 = other
	 */
	addChatMessage: function(side, from, body, timeStamp) {
		var $chatArea = $('#chat-area');
		
		var $message = $('<div class="chat-message"></div>');
		var me = (side === 0) ? 'me' : '';
		
		var date = new Date();
		
		if (timeStamp === null) {
			var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
				(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		}
		
		var bodies = body.split(/\r\n|\r|\n/);
		$message.append(
			'<span class="username ' + me + '">' + WGchat_API.checkRealName(from) + '<span class="right">' + timeStamp + '</span></span>'
		);

		bodies.forEach(function(text) {
			$message.append(
				'<span class="msg-body">' + text + '</span>'
			);
		});
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
	},
	
	addChatNotification: function(body) {
		var $chatArea = $('#chat-area');
		
		var $message = $('<div class="chat-message notification"></div>');
		
		var date = new Date();
		var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
			(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		
		$message.append(
			'<span class="msg-body">(' + timeStamp + ') ' + body + '</span>'
		);
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
	},
	
	scrollChat: function() {
		var $chatArea = $('#chat-area');
		
		$chatArea.scrollTop($chatArea.prop('scrollHeight'));
	},
	
	buildForm: function(fields) {
		var $formArea = $('#form-area');
		$formArea.empty();
		
		// Check value from cookie, if there is a value in cookie, don't show the field
		// auto assign the value to corresponding field.
		var cookieData = Backend.getDataCookie();
		
		fields.forEach(function(field) {
			if (cookieData !== null) {
				var aData = cookieData[field.id];
			}
			
			if (aData !== undefined && aData !== null && aData.trim() !== '') {
				field.value = cookieData[field.id];
			} else {
				var $div = $('<div class="form-field"></div>');
				var requiredString = '';
				
				if (field.required) {
					requiredString = '<span class="field-required">(required)</span>';
				}
				
				$div.append(
					'<label for="' + field.id + '">' + field.label + requiredString + '</label>'
				);
				
				if (field.type === 'text-single') {
					$div.append(
						'<input type="text" id="' + field.id + '" name="' + field.id + '" />'
					);
				} else {
					field.values.forEach(function(value) {
						$div.append(
							'<input type="radio" id="' + field.id + '" name="' + field.id + '" value="' + value + '" />' +
							'&nbsp;' + value + '<br/>'
						);
					});
				}
				
				$formArea.append($div);	
			}	
		});
		
		if ($formArea.children().length > 0) {
			$formArea.append(
				'<div class="form-field">' +
				'<input type="submit" id="form-submit" value="Submit" />' +
				'</div>'
			);
			WGchat_API.setLoaderVisible(false);
			$formArea.show();
			
			// Add form submit handler
			$('#form-submit').off('click');
			$('#form-submit').on('click', function() {
				WGchat_API.validateForm(fields);
			});	
		} else {
			// Autovalidate the form!
			WGchat_API.sendForm(fields, cookieData['userID']);
		}
	},
	
	validateForm: function(fields) {
		var $scope = $('.form-field');
		var isValid = true;
		
		console.log($scope);
		
		fields.forEach(function(field) {
			if (field.value === undefined) {
				var id = field.id;
				var required = field.required;
				var value = null;
				
				var $input = $scope.find('input[name=' + id + ']');
				if (field.type === 'text-single') {
					if ($input.val().trim().length > 0) {
	                    value = $input.val().trim();
	                }
				} else {
					var $input = $scope.find('input[name=' + id + ']:checked');
	                
	                if ($input.length > 0) {
	                    value = $input.val();
	                }
				}
				
				// Check if the field is required
	            if (required && value === null) {
	                isValid = false;
	                $scope.find('label[for=' + id + ']').addClass('form-error');
	            } else {
	                $scope.find('label[for=' + id + ']').removeClass('form-error');
	            }
	            
	            // append value to varFields
	            field.value = value;
			}
		});
		
		if (isValid) {
			WGchat_API.sendForm(fields);
		}
	},
	
	sendForm: function(fields, storedUserId) {
		console.log(fields);
		WGchat_API.setFormVisible(false);
		WGchat_API.setLoaderVisible(true);
		
        // TODO: userID must be taken from cookie!
        var userId = (storedUserId !== undefined) ? storedUserId : Strophe.getNodeFromJid(Backend.connection.jid);
        var wgJid = WGchat_API.workgroup;
        
        // Append few additional field
        fields.push({
            id: 'userID',
            value: userId
        });
        console.error('UserID: ' + userId);
        
        // Store fields to cookie
        fields.forEach(function(field) {
        	var newData = {};
        	newData[field.id] = field.value;
        	
        	Backend.storeDataCookie(newData);
        });
        
        var parentLocation = WGchat_API.location;
        if (parentLocation === undefined) {
        	parentLocation = window.location.href;
        }
        fields.push({
            id: 'location',
            value: parentLocation
        });
        
        var req = JoinQueue.build(Backend.connection.jid, wgJid, userId, fields);
        id = $(req.tree()).attr('id');
        
        Backend.connection.addHandler(WGchat_API.handleQueueStatus, 'http://jabber.org/protocol/workgroup', 'message');
        Backend.connection.addHandler(WGchat_API.handleInvitation, 'jabber:x:conference', 'message');
        Backend.send(req);
	}
};

$(document).ready(function() {
	WGchat_API.onlineWorkgroupCheck();
	
	$(window).unload(function() {
		if (Backend.connection !== null) {
			Backend.connection.pause();
		}
	});
	
	// Get real windows Location
	windowProxy.postMessage({ type: 'getLocation', data: { } });
	
	// Prepare the UI
	// $('#txt-chat-input').watermark('Type message here, and press enter to send');
	
	// var fields = [];
	// fields.push({
		// id: 'username',
		// label: 'Nama:',
		// type: 'text',
		// required: false
	// });
	// fields.push({
		// id: 'Jenis_Kelamin',
		// label: 'Jenis Kelamin Anda:',
		// type: 'radio',
		// values: [
			// 'L',
			// 'P'
		// ],
		// required: true
	// });
		
 	// WGchat_API.setChatVisible(true);
	// WGchat_API.buildForm(fields);
});