/**
 * NOTE Alur halaman Google Analytics!
 * 1. chat-start
 * 2. user-form
 * 3. waiting-for-agent
 * 4. chat-active
 * 5. chat-end
 * 
 * optional
 * - offline-message-sent
 * - chat-transcript-sent
 */
if (WGchat_GLOBAL().isWidget) {
	var windowProxy = new Porthole.WindowProxy('http://' + WGchat_GLOBAL().parentUrl);
	
	// Register an event handler to receive messages;
	windowProxy.addEventListener(function(event) {
		if (event.data.type === 'getLocation') { 
			WGchat_API.location = event.data.data.location;
		} else if (event.data.type === 'chatOpen') {
			if (WGchat_API.firstOpen) {
				if (!WGchat_Backend.isWorkgroupAvailable && _gaq) {
					_gaq.push(['_trackPageview',  AnalyticsPagePath.workgroupOffline]);
					_gaq.push(['acc2._trackPageview', AnalyticsPagePath.workgroupOffline]);
				}
				
				WGchat_API.firstOpen = false;
			}
			
			WGchat_API.isHidden = false;
			WGchat_API.updateUnreadMessage(0);
			
			WGchat_API.scrollChat();
		} else if (event.data.type === 'userInteraction') {
			WGchat_API.userInteraction();
		}
	});
}

var AnalyticsPagePath = {
	chatStart: customPath + '/chat-start',
	userForm: customPath + '/user-form',
	waitingAgent: customPath + '/waiting-for-agent',
	chatActive: customPath + '/chat-active',
	chatEnd: customPath + '/chat-end',
	chatResume: customPath + '/chat-resume',
	sendTranscript: customPath + '/chat-transcript-sent',
	sendOffline: customPath + '/offline-message-sent',
	workgroupOffline: customPath + '/workgroup-offline',
	operatorBusy: customPath + '/chat-failed-operator-busy',
	surveyStart: customPath + '/survey-start',
	surveyEnd: customPath + '/survey-end'
};

var WorkgroupForm = {
    name: 'workgroup-form',
    ns: 'http://jivesoftware.com/protocol/workgroup',
    
    build: function(from, to) {
        return $iq({
            from: from,
            to: to,
            type: 'get',
            id: WGchat_Backend.connection.getUniqueId()
        }).c(WorkgroupForm.name, {
            xmlns: WorkgroupForm.ns
        });
    }
};

var JoinQueue = {
    name: 'join-queue',
    ns: 'http://jabber.org/protocol/workgroup',
    
    build: function(from, to, userId, data) {
        var iq = $iq({
            from: from,
            to: to,
            type: 'set',
            id: WGchat_Backend.connection.getUniqueId()
        });
        
        var joinq = iq.c(JoinQueue.name, {xmlns: JoinQueue.ns})
            .c('queue-notifications').up()
            .c('user', {xmlns: JoinQueue.ns, id: userId}).up();
        
        var fields = joinq.c('x', {xmlns: 'jabber:x:data', type: 'submit'});
        $.each(data, function(elemidx, elem) {
            fields.c('field', {'var': elem.id, type: 'text-single'}).c('value').t(elem.value).up().up();
        });
        
        return iq;
    }
};

var SurveyForm = {
    name: 'survey-form',
    ns: 'http://jivesoftware.com/protocol/workgroup',
    
    build: function(from, to) {
        return $iq({
            from: from,
            to: to,
            type: 'get',
            id: WGchat_Backend.connection.getUniqueId()
        }).c(SurveyForm.name, {
            xmlns: SurveyForm.ns
        });
    }
};

var SurveyAnswer = {
    name: 'survey-form-save',
    ns: 'http://jabber.org/protocol/workgroup',
    
    build: function(from, to, sessionID, data) {
        var iq = $iq({
            from: from,
            to: to,
            type: 'set',
            id: WGchat_Backend.connection.getUniqueId()
        });
        
        var answerq = iq.c(SurveyAnswer.name, {xmlns: SurveyAnswer.ns, 'sessionID': sessionID});
        var fields = answerq.c('x', {xmlns: 'jabber:x:data', type: 'submit'});
        $.each(data, function(elemidx, elem) {
        	if (elem.value !== undefined && $.trim(elem.value).length > 0) {
        		fields.c('field', {'var': elem.id, type: 'text-single'}).c('value').t(elem.value).up().up();	
        	}
        });
        
        return iq;
    }
};

var WGchat_Backend = {
	boshUrl:  WGchat_GLOBAL().boshUrl,//'http://kemass-macbook-air.local:8080/http-bind/',
    xmppDomain:  WGchat_GLOBAL().xmppDomain,
    resource: 'WGwidget',
    connection: null,
    isWorkgroupAvailable: false,
    isAttachmentFailed: true,
    pingInterval:  5 * 60 * 1000, // in miliseconds
    pingTimer: null,
    
    openConnection: function(handler) {
       	WGchat_Backend.connection = new Strophe.Connection(WGchat_Backend.boshUrl);
       	WGchat_Backend.connection.connect(WGchat_Backend.xmppDomain + '/' + WGchat_Backend.resource, '', handler);
        
        // WGchat_Backend.connection.addHandler(WGchat_Backend.onPacketLog);
        
        // HACK for IE that detect BOSH disconnect as window.unload
        setTimeout(function () {
        	if (WGchat_Backend.connection) {
        		WGchat_Backend.connection.resume();	
        	}
        }, 1000);
    },
    
    attachConnection: function(handler) {
        var jid = $.cookie('wgchat-jid');
        var sid = $.cookie('wgchat-sid');
        var rid = $.cookie('wgchat-rid');
        
        WGchat_Backend.connection = new Strophe.Connection(WGchat_Backend.boshUrl);
        WGchat_Backend.connection.resume();
        WGchat_Backend.connection.attach(jid, sid, parseInt(rid) + 0, handler);
        
        WGchat_Backend.connection.addHandler(WGchat_Backend.onPacketLog);
        
        // HACK for IE that detect BOSH disconnect as window.unload
        setTimeout(function () {
        	if (WGchat_Backend.connection) {
        		WGchat_Backend.connection.resume();	
        	}
        }, 1000);
    },
    
    closeConnection: function(reason) {
    	if (reason === undefined) {
    		reason = 'disconnect';
    	}
    	
        if (WGchat_Backend.connection !== null) {
            WGchat_Backend.connection.disconnect(reason);
            WGchat_Backend.connection.reset();
            
            WGchat_Backend.connection = null;
            
            // console.log('disconnected: ' + WGchat_Backend.connection);
        }
    },
    
    isCookieAvailable: function() {
        var room = $.cookie('wgchat-roomjid');
        var jid = $.cookie('wgchat-jid');
        var sid = $.cookie('wgchat-sid');
        var rid = $.cookie('wgchat-rid');
        
        if (room !== null && jid !== null && sid !== null && rid !== null) {
            return true;
        } else {
            WGchat_Backend.resetCookie();
            return false;
        }
    },
    
    resetCookie: function() {
        $.cookie('wgchat-roomjid', null, {path: location.pathname});
        $.cookie('wgchat-jid', null, {path: location.pathname});
        $.cookie('wgchat-sid', null, {path: location.pathname});
        $.cookie('wgchat-rid', null, {path: location.pathname});
    },
    
    onPacketLog: function(packet) {
    	if (console && console.log) {
    		console.log('LOG Packet: ' + (new Date()));
        	console.log(packet);	
    	}
        
        return true;
    },
    
    handlePingIq: function(packet) {
    	var $packet = $(packet);
    	var from 	= $packet.attr('from');
    	var to		= $packet.attr('to');
    	var id		= $packet.attr('id');
    	
    	var result = $iq({
            from: to,
            to: from,
            type: 'result',
            id: id
        });
        
        WGchat_Backend.send(result);
    	
    	return true;
    },
    
    initPinger: function() {
		WGchat_Backend.pingTimer = setInterval(function () {
			if (WGchat_Backend.connection === null) {
				clearInterval(WGchat_Backend.pingTimer);
				WGchat_Backend.pingTimer = null;
				
				return;
			}
			
			var pingIq = $iq({
	            type: 'get',
	            id: WGchat_Backend.connection.getUniqueId()
	        }).c('ping', {
	            xmlns: 'urn:xmpp:ping'
	        });
	        
	        WGchat_Backend.send(pingIq);
			
		}, WGchat_Backend.pingInterval);
	},
	
	removePinger: function() {
		if (WGchat_Backend.pingTimer !== null) {
			clearInterval(WGchat_Backend.pingTimer);
			WGchat_Backend.pingTimer = null;
		}
	},
    
    handleRidUpdate: function(packet) {
    	var curDate = new Date();
		var expDate = new Date(curDate.getTime() + (21600000));
			
        $.cookie('wgchat-rid', WGchat_Backend.connection.rid, { expires: expDate, path: location.pathname });
        
        return true;
    },
    
    send: function(packet) {
    	WGchat_Backend.connection.resume();
    	WGchat_Backend.connection.send(packet);
    	
    	// console.log('SENT: ' + packet.toString());
    },
    
    getDataCookie: function() {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			return JSON.parse(existingData);
    		}
    	}
    	
    	return null;
    },
    
    /**
     * @param data object with key value pair
     */
    storeDataCookie: function(data) {
    	if (JSON !== undefined) {
    		var existingData = $.cookie('wgchat-form-data');
    		
    		if (existingData !== null) {
    			existingData = JSON.parse(existingData);
    			existingData = WGchat_Backend.mergeOptions(existingData, data);
    		} else {
    			existingData = data;
    		}
    		
    		// console.log(existingData);
    		
    		$.cookie('wgchat-form-data', JSON.stringify(existingData), { expires: 30, path: location.pathname });
    	}
    },

	clearDataCookie: function() {
		var storedData = WGchat_Backend.getDataCookie();
		
		if (storedData !== null && storedData.userID !== undefined) {
			var userId = storedData.userID;
		}
		
		$.cookie('wgchat-form-data', null);
		
		// Restore the userID
		WGchat_Backend.storeDataCookie({
			userID: userId
		});
	},
    
    /**
     * http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically
	 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
	 * @param obj1
	 * @param obj2
	 * @returns obj3 a new object based on obj1 and obj2
	 */
	mergeOptions: function(obj1,obj2){
	    var obj3 = {};
	    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	    return obj3;
	}
};

var WGchat_API = {
	workgroup: WGchat_GLOBAL().workgroup,
	roomOccupants: 0,
	isActive: false,
	firstOpen: true,
	
	roomJoinedHandler: null,
	roomjid: null,
	nickname: null,
	agentNodes: {},
	historyBuffer: '',
	cookieTimeout: 6 * 60 * 60 * 1000,
	
	unreadMessage: 0,
	isHidden: false,
	isAttached: false,
	surveyFormAvailable: false,
	
	groupPresenceHandlerRef: null,
	
	resetData: function() {
		WGchat_API.isActive = false;
		WGchat_API.roomJoinedHandler = null;
		WGchat_API.roomjid = null;
		WGchat_API.nickname = null;
		WGchat_API.roomOccupants = 0;
		WGchat_API.agentNodes = {};
	},
	
	onlineWorkgroupCheck: function() {
	   	if (WGchat_Backend.isCookieAvailable()) {
        	WGchat_API.setLoaderVisible(true, 'Resuming chat...');
       	}
       	
       	$('#chat-intro').addClass('hidden');
		
        WGchat_Backend.openConnection(WGchat_API.workgroupCheckConnectionHandler);
    },
	
	/*
	 * HANDLER REGION
	 */
	workgroupCheckConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTED) {
            // try to send presence to workgruop
            WGchat_Backend.connection.addHandler(WGchat_API.handleWorkgroupPresence, 
            	'http://jivesoftware.com/protocol/workgroup', 'presence');
            
            var pres = $pres({
            	id: WGchat_Backend.connection.getUniqueId(),
            	from: WGchat_Backend.connection.jid, 
            	to: WGchat_API.workgroup
            });
            
            WGchat_Backend.send(pres);
        } else if (status === Strophe.Status.AUTHENTICATING) {
            // console.log('Authenticated!');
        } else if (status === Strophe.Status.CONNFAIL) {
            // console.log('Workgroup Check connection failed: ' + msg)
        } else if (status === Strophe.Status.AUTHFAIL) {
            // console.log('Workgroup Check authentication failed: ' + msg)
        } else if (status === Strophe.Status.DISCONNECTED) {
            // console.log('Workgroup check closed: ' + msg);
        }
    },
    
    handleWorkgroupPresence: function(presence) {
        WGchat_Backend.closeConnection();
    	
    	WGchat_API.setLoaderVisible(false);
    	
        var isAvailable = $(presence).attr('type') !== 'unavailable';
        WGchat_Backend.isWorkgroupAvailable = isAvailable;
        
        if (isAvailable) {
        	$('#offline-form').remove();
        	
            if (WGchat_Backend.isCookieAvailable()) {
            	$('#chat-intro').addClass('hidden');
				
				WGchat_API.resumeChat();
            } else {
            	$('#chat-intro').removeClass('hidden');
				$('#btn-start-chat').on('click', WGchat_API.startChat);
            }
        } else {
        	var headerText 	= $('#header .text');
        	var offlineForm = $('#offline-form');
        	
        	headerText.text('[Offline] ' + headerText.text());
        	offlineForm.removeClass('hidden');
        	WGchat_Backend.resetCookie();
        	
        	offlineForm.find('[type=submit]').click(function(evt) {
        		evt.preventDefault();
        		evt.stopPropagation();
        		
        		offlineForm.find('.alert').remove();
        		
        		var name 	= offlineForm.find('#offline-username').val();
        		var email 	= offlineForm.find('#offline-email').val();
        		var message	= offlineForm.find('#offline-message').val();
        		
        		WGchat_API.sendOfflineMessage(offlineForm, $(evt.currentTarget), name, email, message);
        	});
        	
        	if (!windowProxy && _gaq) {
        		_gaq.push(['_trackPageview', AnalyticsPagePath.workgroupOffline]);
				_gaq.push(['acc2._trackPageview', AnalyticsPagePath.workgroupOffline]);	
        	}
        }
        
        if (windowProxy) {
        	WGchat_API.isHidden = true;
        	windowProxy.post({ type: 'frameReady', data: {
				headerText: WGchat_GLOBAL().widgetTitle,
				isAvailable: isAvailable,
				isNewComer: WGchat_API.isNewComer()
			} });
			
			$('#header').on('click', function() {
				WGchat_API.isHidden = true;
		    	windowProxy.post({ type: 'hideChat', data: {} });
			});
		}
    },
    
    mainConnectionHandler: function(status, msg) {
        if (status === Strophe.Status.CONNECTING) {
            // console.log('Connecting...');
        } else if (status === Strophe.Status.AUTHENTICATING) {
            // console.log('Authenticated!');
        } else if (status === Strophe.Status.CONNECTED) {
        	// console.log('Connected!');
            // Try to initiate chat with support
            var domain = Strophe.getDomainFromJid(WGchat_Backend.connection.jid);
            var formReq = WorkgroupForm.build(WGchat_Backend.connection.jid, WGchat_API.workgroup);
            var id = $(formReq.tree()).attr('id');
            
            WGchat_Backend.connection.addHandler(WGchat_API.handleWorkgroupForm, WorkgroupForm.ns, 'iq', null, id);
            WGchat_Backend.send(formReq);
            
            // Add iq ping handler
            // REPLACED BY CLIENT TO SERVER PING!
            WGchat_Backend.connection.addHandler(WGchat_Backend.handlePingIq, 'urn:xmpp:ping', 'iq');
            
            WGchat_Backend.initPinger();
        } else if (status === Strophe.Status.ATTACHED) {
            // console.log('Connection attached.');
            WGchat_API.isAttached = true;
            
            // Group presence handler
        	WGchat_API.groupPresenceHandlerRef = WGchat_Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
            WGchat_API.roomjid = $.cookie('wgchat-roomjid');
			WGchat_Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            	'presence', null, null, WGchat_API.roomjid);
			
			// Defer the presence sending for 500 miliseconds
			setTimeout(function() {
				WGchat_Backend.send($pres({to: WGchat_API.roomjid, type: 'unavailable'}).
	                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
	            
	            // Send available presence after unavailable request is responded by server
	           	WGchat_Backend.connection.addHandler(function() {
	           		WGchat_Backend.send($pres({to: WGchat_API.roomjid}).
		                c('x', {xmlns: 'http://jabber.org/protocol/muc'}));
		            
		            return false;
	           	}, 'jabber:client', 'presence', 'unavailable');
			}, 500);
	            
            // Add iq ping handler
            // REPLACED BY CLIENT TO SERVER PING!
            WGchat_Backend.connection.addHandler(WGchat_Backend.handlePingIq, 'urn:xmpp:ping', 'iq');
            
            WGchat_Backend.initPinger();
        } else if (status === Strophe.Status.CONNFAIL) {
            // console.log('Connection failed. REASON: ' + msg);
            // WGchat_Backend.resetCookie();
        } else if (status === Strophe.Status.DISCONNECTED) {
            // console.log('Connection terminated. REASON: ' + msg);
            WGchat_Backend.removePinger();
            
            if (WGchat_Backend.isAttachmentFailed === true) {
            	WGchat_API.setLoaderVisible(false);
	    		WGchat_API.setChatVisible(true);
	    		WGchat_API.closeChat('Chat session closed by operator.<br/><br/><span><a class="btn btn-small" id="btn-restart-chat" href="javascript:void(null)"><i class="icon-comment"></i>&nbsp;Chat Again</a>');
            }
        }
    },
    
    // formBuilderHelper
    parseFormPacket: function(packet) {
    	var fields = $(packet).find('field');
        var varFields = null;
        
        if (fields) {
        	varFields = [];
        	fields.each(function(index, elem) {
	            var $elem = $(elem);
	            
	            if ($elem.attr('type') !== 'hidden') {
	                var name = $elem.attr('var');
	                var label = $elem.attr('label');
	                var type = $elem.attr('type');
	                var required = $(elem).find('required').length > 0 || name === 'username';
	                var values = [];
	                
	                if ($elem.attr('type') === 'list-multi') {
	                    $elem.find('option').each(function(idx, elm) {
	                    	var $elm = $(elm);
	                    	
	                    	values.push($elm.find('value').text());
	                    });
	                }
	                
	                varFields.push({
	                    id: name,
	                    label: label,
	                    type: type,
	                    values: values,
	                    required: required
	                });
	            }
	        });
        }
        
        return varFields;
    },
    
    handleWorkgroupSurveyForm: function(packet) {
    	var varFields = WGchat_API.parseFormPacket(packet);
        
        if (varFields !== null && varFields.length > 0) {
        	
        	// Survey form existed, build it!
        	var $formTag = WGchat_API.buildForm(varFields);
	        var $formArea = $('#form-area');
			$formArea.empty();
			
			$formTag.append(
				'<div class="control-group">' +
					'<input class="btn btn-primary" type="submit" id="form-submit" value="Submit Survey" />' +
				'</div>'
			);
			$formArea.append($formTag);
			
			// Add form submit handler
			$('#form-submit').off('click');
			$('#form-submit').on('click', function(evt) {
				evt.stopPropagation();
				evt.preventDefault();
				
				var sessionId = Strophe.getNodeFromJid(WGchat_API.roomjid);
				if (sessionId) {
					if (WGchat_API.validateForm(varFields))  {
						WGchat_API.sendSurveyForm(varFields, sessionId);
					}
				} else {
					throw 'No sessionID is found, please try again.';	
				}
			});
			
			WGchat_API.surveyFormAvailable = true;
        }
    },
    
    handleWorkgroupForm: function(packet) {
    	var fields = $(packet).find('field');
    	var varFields = WGchat_API.parseFormPacket(packet);
        
        // Check wheter email is exists
        var isEmailExisted = false;
        for(var i = 0, j = varFields.length; i < j; i++){
        	var checkedField = varFields[i];
        	if (checkedField.id === 'email') {
        		isEmailExisted = true;
        	}
        };
        
        if (!isEmailExisted) {
        	// Add email to the form (mandatory)
        	varFields.push({
                id: 'email',
                label: 'Email',
                type: 'text-single',
                values: [],
                required: true
            });
        }
        
        var $formTag = WGchat_API.buildForm(varFields);
        var $formArea = $('#form-area');
		$formArea.empty();
		
        if ($formTag.children().length > 0) {
			$formTag.append(
				'<div class="control-group">' +
					'<input class="btn btn-primary" type="submit" id="form-submit" value="Submit" />' +
				'</div>'
			);
			$formArea.append($formTag);
			
			WGchat_API.setLoaderVisible(false);
			WGchat_API.setFormVisible(true);
			
			// Add form submit handler
			$('#form-submit').off('click');
			$('#form-submit').on('click', function(evt) {
				evt.stopPropagation();
				evt.preventDefault();
				
				if (WGchat_API.validateForm(varFields))  {
					WGchat_API.sendForm(varFields);
				}
			});	
		} else {
			// Autovalidate the form!
			WGchat_API.sendForm(fields, cookieData['userID']);
		}
		
		if (_gaq) {
			_gaq.push(['_trackPageview', AnalyticsPagePath.userForm]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.userForm]);
		}
    },
    
    handleInvitation: function(packet) {
    	var room = $(packet).attr('from');
        var nickname = 'Customer';
        
        // Send online presence
        WGchat_Backend.send($pres().c('status').t('online').up().c('priority').t('1'));
        
        WGchat_API.acceptInvitation(room, nickname);
    },
    
    handleInvitationError: function(packet) {
        var jid = $(packet).attr('from');
        var room = Strophe.getBareJidFromJid(jid);
        var nickname = Strophe.getResourceFromJid(jid);
        
        WGchat_API.acceptInvitation(room, nickname + '-' + WGchat_Backend.connection.getUniqueId());
    },
    
    handleRoomJoined: function(packet) {
    	// set the attachmentfialed to false
    	WGchat_Backend.isAttachmentFailed = false;
    	if (WGchat_API.isAttached && _gaq) {
			_gaq.push(['_trackPageview', AnalyticsPagePath.chatResume]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.chatResume]);
		}
		
        WGchat_API.roomjid = $(packet).attr('from');
        WGchat_API.nickname = Strophe.getResourceFromJid($(packet).attr('from'));
        
        // Write to cookie
        WGchat_API.updateConnectionCookie();
        
        // Must add the cookie RID updater
        WGchat_Backend.connection.addHandler(WGchat_Backend.handleRidUpdate);
        
        // Live message handler
        WGchat_Backend.connection.addHandler(WGchat_API.handleLiveMessage, null, 'message', 'groupchat');
        
        WGchat_API.setLoaderVisible(false);
        WGchat_API.setChatVisible(true);
        
        // Add event handler to chat input box
        $('#txt-chat-input').on('keypress', WGchat_API.handleChatboxInput);
        
        if (windowProxy) {
        	windowProxy.post({ type: 'openChat', data: { }});
        	WGchat_API.userInteraction;
        }
        
        if (_gaq) {
        	// Defer the chat active tracking, just to make sure
			setTimeout(function() {
				_gaq.push(['_trackPageview',  AnalyticsPagePath.chatActive]);
				_gaq.push(['acc2._trackPageview', AnalyticsPagePath.chatActive]);
			}, 3000);
		}
		
		// Get the survey form, for preparation!
		var surveyReq = SurveyForm.build(WGchat_Backend.connection.jid, WGchat_API.workgroup);
        var id = $(surveyReq.tree()).attr('id');
        
        WGchat_Backend.connection.addHandler(WGchat_API.handleWorkgroupSurveyForm, SurveyForm.ns, 'iq', null, id);
        WGchat_Backend.send(surveyReq);
	},
    
    handleGroupPresence: function(packet) {
    	var isCustomer 	= $(packet).attr('from').indexOf("Customer") !== -1;
    	var isLeft 		= $(packet).attr('type') === 'unavailable';
    	
        if (isLeft) {
            WGchat_API.roomOccupants--;
            
            if (WGchat_API.roomOccupants < 0) {
                WGchat_API.roomOccupants = 0;
            }
        } else {
            WGchat_API.roomOccupants++;
            
            if (WGchat_API.roomOccupants > 1) {
                WGchat_API.isActive = true;
            }
            
            // Request other user vcard!
            if (!isCustomer) {
            	/**
					<iq from='stpeter@jabber.org/roundabout'
					    id='v3'
					    to='jer@jabber.org'
					    type='get'>
					  <vCard xmlns='vcard-temp'/>
					</iq>
            	 */
            	
            	var iqFrom	= WGchat_Backend.connection.jid;
            	var iqTo 	= Strophe.getResourceFromJid($(packet).attr('from')) + '@' + WGchat_Backend.xmppDomain;
            	var id 		= WGchat_Backend.connection.getUniqueId();
            	var iq		= $iq({ from: iqFrom, to: iqTo, id: id, type: 'get' }).c('vCard', { xmlns: 'vcard-temp' });
            	
            	// // console.error(iq.toString());
            	
            	// WGchat_Backend.send(iq); 
            }
        }
        
        var chatEnded 	= false;
        var timeout 	= 0;
        if (WGchat_API.isActive && isLeft) {
        	if (isCustomer) {
        		chatEnded = true;
        	} else if (WGchat_API.roomOccupants === 1) {
        		chatEnded = true;
        		timeout = 7000;	
        	}
        }
        
        if (chatEnded) {
        	var timeoutHandler = setTimeout(function() {
        		if (WGchat_API.roomOccupants <= 1) {
	           		WGchat_API.roomOccupants = 0;
	            	WGchat_API.closeChat();
	            	
	            	try {
	            		WGchat_Backend.connection.deleteHandler(WGchat_API.groupPresenceHandlerRef);	
	            	} catch (e) { }	
	            	WGchat_API.groupPresenceHandlerRef = null;
            	}
        	}, timeout);
        }
        
        return true;
    },
   
	handleLiveMessage: function(packet) {
		var jidNick = Strophe.getResourceFromJid($(packet).attr('from'));
		var $packet = $(packet);
		
        // Check whether the conference is closed
        if (jidNick === null) {
        	WGchat_API.closeChat('Chat session closed by operator.<br/><br/><span><a class="btn btn-small" id="btn-restart-chat" href="javascript:void(null)"><i class="icon-comment"></i>&nbsp;Chat Again</a>');
        	
            return false;
        } else if ($packet.find('x').size() === 0) {
        	return true;
        }
    
        if ($(packet).find('body').size() > 0) {
            // message received
            var body = $(packet).find('body').text();
            var isMe = (WGchat_API.nickname === jidNick) ? 0 : 1;
            
            if (isMe === 0) {
            	jidNick = 'Me';
            }
            
            // Check timestamp in body
            var timeStamp = null;
            if ($(packet).find('delay').length > 0 
            	&& $(packet).find('delay').attr('stamp') !== null) {
            	
            	var rawStamp = $(packet).find('delay').attr('stamp').split('T')[1].split(':');
            	var gmtHours = -(new Date()).getTimezoneOffset()/60;
            	
            	timeStamp = (parseInt(rawStamp[0], 10) + gmtHours) + ':' + rawStamp[1];
            } else {
            	// Play sound for new message
            	// soundManager.onready(function() {
					// soundManager.play('incomingchat');
				// });
            }
            
            // append message
            setTimeout(function() {
            	WGchat_API.addChatMessage(isMe, jidNick, body, timeStamp);	
            }, 250);
        }
        
        // Update unread Message if not opened
        if (WGchat_API.isHidden) {
        	WGchat_API.updateUnreadMessage(WGchat_API.unreadMessage + 1);
        }
        
        return true;
	},
    
    handleChatboxInput: function(evt) {
    	var $txtArea = $(evt.target);
    	
    	if (evt.keyCode == 13 && evt.shiftKey == 0) {
    		var message = $txtArea.val();
    		message = message.replace(/^\s+|\s+$/g,"");
    		
    		$txtArea.val('');
    		$txtArea.focus();
    		
    		if ($.trim(message.toString()) !== '') {
    			var jid = Strophe.getBareJidFromJid(WGchat_API.roomjid);
                
                WGchat_Backend.send($msg({
                    to: jid,
                    'type': 'groupchat'
                }).c('body').t(message));
                
                WGchat_API.addChatMessage(0, 'Me', message);
    		}
    		
    		return false;
    	}
    },
    
    handleQueueStatus: function(packet) {
    	// console.log(packet);
    	
    	if ($(packet).find('depart-queue').length > 0 &&
    		$(packet).find('depart-queue').find('agent-not-found').length > 0) {
    		
    		// Show queue full notification!
    		WGchat_API.setLoaderVisible(false);
    		WGchat_API.setChatVisible(true);
    		WGchat_API.closeChat('Sorry, no operator available. <span><a id="btn-restart-chat" href="javascript:void(null)">Please try again later.</a></span>');
    		
    		if (_gaq) {
				_gaq.push(['_trackPageview', AnalyticsPagePath.operatorBusy]);
				_gaq.push(['acc2._trackPageview', AnalyticsPagePath.operatorBusy]);
			}
    		
    		return false;
    	}
    	
    	return true;
    },
    
	/**
	 * END OF HANDLER REGION
	 */
	
	acceptInvitation: function(room, nickname) {
        var id = WGchat_Backend.connection.getUniqueId();
    
        // Add error handler
        WGchat_Backend.connection.addHandler(WGchat_API.handleInvitationError, 'http://jabber.org/protocol/muc', 'presence', 'error', null, id);
        
        // Add room joined handler, check whether it is not null
        if (WGchat_API.roomJoinedHandler !== null) {
            // Strophe.deleteHandler(WGchat_API.roomJoinedHandler);
        }
        WGchat_API.roomJoinedHandler = WGchat_Backend.connection.addHandler(WGchat_API.handleRoomJoined, 'http://jabber.org/protocol/muc#user', 
            'presence', null, null, room + '/' + nickname);
        
        // Group presence handler
        WGchat_API.groupPresenceHandlerRef = WGchat_Backend.connection.addHandler(WGchat_API.handleGroupPresence, 'http://jabber.org/protocol/muc#user', 'presence');    
        
        var pres = $pres({to: room + '/' + nickname, id: id}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
        WGchat_Backend.send(pres);
    },
	
	setLoaderVisible: function(visible, loaderText) {
		var defaultText = "Loading...";
		var $loader = $('#loading-indicator');
		
		if (visible) {
			$loader.removeClass('hidden');
			
			var textContainer = $('#loading-text');
			textContainer.html('<h3>' + loaderText || defaultText + '</h3>');
		} else {
			$loader.addClass('hidden');
		}
	},
	
	/**
	 * @param visible show / hide the chat box
	 * @param persistChat whether to persist the previous chat or reset it
	 * @param showTextBox whether to show the textbox where the user can type their message
	 */
	setChatVisible: function(visible, persistChat, showTextBox) {
		var $chat = $('#chat-area, #chat-input-area');
		
		if (visible) {
			$chat.removeClass('hidden');
			if (!persistChat) {
				$('#inner-chat-area').empty();	
				
				$('#btn-close-chat').off('click');
				$('#btn-close-chat').on('click', WGchat_API.endChat);
				$('#btn-close-chat').removeClass('hidden');
			}
			$('#chat-area').removeClass('full-chat-area');
			
			if (showTextBox === false) {
				$('#chat-input-area').addClass('hidden');
			} else {
				$('#chat-input-area').removeClass('hidden');
				$('#txt-chat-input').watermark('Type message here, and press enter to send');
			}
			
			WGchat_API.scrollChat(true);
			WGchat_API.resizeChatArea();
		} else {
			$chat.addClass('hidden');
			
			$('#btn-close-chat').off('click');
			$('#btn-close-chat').addClass('hidden');
		}
	},
	
	setFormVisible: function(visible) {
		var $form = $('#form-area');
		
		if (visible) {
			$form.removeClass('hidden');
		} else {
			$form.addClass('hidden');
		}
	},
	
	resumeChat: function() {
		WGchat_API.setLoaderVisible(true, 'Resuming chat...');
		
		WGchat_Backend.attachConnection(WGchat_API.mainConnectionHandler);
	},
	
	startChat: function() {
		WGchat_API.scrollChat(true);
		
		WGchat_API.setChatVisible(false);
		WGchat_API.setFormVisible(false);
		
		$('#chat-intro').addClass('hidden');
		WGchat_API.setLoaderVisible(true, 'Starting chat session...');
		
		// reset the data
		WGchat_API.resetData();
		
		WGchat_Backend.openConnection(WGchat_API.mainConnectionHandler);
		
		if (_gaq) {
			_gaq.push(['_trackPageview', AnalyticsPagePath.chatStart]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.chatStart]);
		}
		
		// Mark user interaction
		WGchat_API.userInteraction();
	},
	
	endChat: function(evt) {
		if (evt !== undefined) {
			evt.stopImmediatePropagation();	
		}
		
		var confAnswer = confirm('Want to close this chat?');
		if (confAnswer) {
			var id = WGchat_Backend.connection.getUniqueId();
        
	        var pres = $pres({to: WGchat_API.roomjid, id: id, type: 'unavailable'}).c('x', {xmlns: 'http://jabber.org/protocol/muc'});
	        WGchat_Backend.send(pres);
	        
	        WGchat_API.closeChat();
		}
	},
	
	validateEmail: function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	   
	    return re.test(email);
	}, 
	
	closeChat: function(text) {
		
		if (text === undefined) {
			text = 'Chat ended.<br/><br/><span><a class="btn btn-small" id="btn-restart-chat" href="javascript:void(null)"><i class="icon-comment"></i>&nbsp;Chat Again</a>' + 
				'&nbsp;<a class="btn btn-small btn-primary" id="btn-send-transcript" href="javascript:void(null)"><i class="icon-envelope icon-white"></i>&nbsp;Send Transcript</a>'
			
			if ( ! WGchat_GLOBAL().isWidget && WGchat_GLOBAL().userData === null) {
				// Add close window button if not widget
				text += '&nbsp;<a class="btn btn-small btn-danger" id="btn-close-window" href="javascript:void(null)"><i class="icon-remove icon-white"></i>&nbsp;Close</a>';
			}
			
			text += '</span>';
		}
		
		/** commented, bypass survey button 
		if (WGchat_API.surveyFormAvailable) {
			text += 
			'<br/><br/><span>' +
				'<a class="btn btn-small btn-warning" id="btn-start-survey" href="javascript:void(null)"><i class="icon-tasks icon-white"></i>&nbsp;Take Survey</a>' + 
			'</span>';
		}
		//*/
		
		WGchat_API.addChatNotification(text);
		
		$('#chat-input-area').off('keypress');
		$('#chat-input-area').addClass('hidden');
		
		$('#btn-close-chat').addClass('hidden');
		
		$('#btn-restart-chat').off('click');
		$('#btn-send-transcript').off('click');
		$('#btn-close-window').off('click');
		
		$('#btn-restart-chat').on('click', WGchat_API.startChat);
		$('#btn-send-transcript').on('click', WGchat_API.sendTranscript);
		// $('#btn-start-survey').on('click', WGchat_API.startSurvey);
		$('#btn-close-window').on('click', function() {
			window.close();
		});
		
		WGchat_API.resizeChatArea(true);
		
		if (_gaq) {
			_gaq.push(['_trackPageview', AnalyticsPagePath.chatEnd]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.chatEnd]);
		}
		
		WGchat_Backend.resetCookie();
        WGchat_Backend.closeConnection();
        
        // always show the survey form
        if (WGchat_API.surveyFormAvailable) {
        	WGchat_API.startSurvey();
        }
	},
	
	startSurvey: function() {
		if ($('#btn-start-survey')) {
			$('#btn-start-survey').remove();	
		}
		
		WGchat_API.setFormVisible(true);
		WGchat_API.setChatVisible(false, true);
		
		if (_gaq) {
			_gaq.push(['_trackPageview',  AnalyticsPagePath.surveyStart]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.surveyStart]);
		}
	},
	
	sendOfflineMessage: function(formContainer, button, name, email, message) {
		var postData = {
			name: name,
			email: email,
			message: message,
			'api_key': WGchat_GLOBAL().apiKey,
			'parent_url': WGchat_GLOBAL().parentUrl
		};
		
		var jqxhr = $.post(WGchat_GLOBAL().baseUrl + 'chatwidget/offline_message', postData,			
			function(data) {
				formContainer.find('.control-group input, .control-group textarea').attr('disabled', '');
		
				button.addClass('btn-success disabled');
				button.html('<i class="icon-white icon-ok"></i>&nbsp;Message sent!');
				
				if (_gaq) {
					_gaq.push(['_trackPageview', AnalyticsPagePath.sendOffline]);
					_gaq.push(['acc2._trackPageview', AnalyticsPagePath.sendOffline]);
				}
	    	}, 'json')
	    .error(function(data) {
	    	data = $.parseJSON(data.responseText);
	    	
    		formContainer.append(
    			'<div class="alert alert-error">' + 
    				'<button class="close" data-dismiss="alert">x</button>' + data.message + 
    			'</div>');
    		
    		button.button('reset');
	    });	
		
		button.button('loading');
	},
	
	sendTranscript: function(evt) {
		var $button = $(evt.currentTarget);
		if ($button.attr('class').indexOf('disabled') !== -1) {
			return;
		}
		
		$button.button();
		$button.button('loading');
		
		// Check email address in cookie
		var cookieData = WGchat_Backend.getDataCookie();
		var emailAddress = cookieData['email'] || '';
		
		var emailAddress = prompt("Enter your email address: ", emailAddress);
		if (emailAddress === null) {
			$button.button('reset');
			return;
		}
		
		if ($.trim(emailAddress.toString()).length > 0 && WGchat_API.validateEmail(emailAddress)) {
			var headerText 	= $('#header .text');
			var date = new Date();
			var postData = { 
				email: emailAddress, 
				transcript: WGchat_API.historyBuffer,
				'api_key': WGchat_GLOBAL().apiKey,
				'parent_url': WGchat_GLOBAL().parentUrl,
				'workgroup_description': headerText.text()
			};
			
			WGchat_API.historyBuffer += '\nDate: ' + date.toString() + '\n=== generated by WGchat.com - Empowering Online Chat';
			
			var jqxhr = $.post(WGchat_GLOBAL().baseUrl + 'chatwidget/transcript', 
				postData,			
				function(data) {
					WGchat_API.addChatNotification(data.message, 'success');
					
					// Remove btn start chat after transcript sending succeed
					var $btnSend = $('#btn-send-transcript');
					$btnSend.remove();
					
					if (_gaq) {
						_gaq.push(['_trackPageview',  AnalyticsPagePath.sendTranscript]);
						_gaq.push(['acc2._trackPageview', AnalyticsPagePath.sendTranscript]);
					}
		    	}, 'json')
		    .error(function(data) {
		    	$button.button('reset');
		    	data = $.parseJSON(data.responseText);
		    
		    	WGchat_API.addChatNotification(data.message, 'error');
		    });	
		    
		    // Store email address in cookie
		    WGchat_Backend.storeDataCookie({
		    	'email': emailAddress
		    });
		} else {
			$button.button('reset');
			WGchat_API.addChatNotification("Invalid email address, please retry using valid email address.", 'error');
		}
	},
	
	checkRealName: function(jidNode) {
		var name = WGchat_API.agentNodes[jidNode];
		
		// return (name !== undefined) ? name : jidNode;
		
		return jidNode.split('-')[0];
	},
	
	/**
	 * side = 0 / 1, 0 = me, 1 = other
	 */
	addChatMessage: function(side, from, body, timeStamp) {
		var $chatArea = $('#inner-chat-area');
		
		var $message = $('<div class="chat-message"></div>');
		var me = (side === 0) ? 'me' : '';
		
		var date = new Date();
		
		if (!timeStamp) {
			var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
				(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		}
		
		var bodies = body.split(/\r\n|\r|\n/);
		$message.append(
			'<span class="username ' + me + '">' + WGchat_API.checkRealName(from) + '<span class="right">' + timeStamp + '</span></span>'
		);

		$.each(bodies, function(textidx, text) {
			var formattedText = WGchat_API.formatUrl(text);
			
			$message.append(
				'<span class="msg-body">' + formattedText + '</span>'
			);
		});
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
		
		// Save history to buffer
		WGchat_API.historyBuffer += WGchat_API.checkRealName(from) + ' (' + timeStamp + '): ' + body + '\n\n';
	},
	/**
	 * @author http://stackoverflow.com/a/3890175
	 */
	formatUrl: function(inputText) {
		var replaceText, replacePattern1, replacePattern2, replacePattern3;

	    //URLs starting with http://, https://, or ftp://
	    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
	    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');
	
	    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
	    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
	    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');
	
	    //Change email addresses to mailto:: links.
	    replacePattern3 = /([\w.]+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})/gim;
	    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1" target="_blank">$1</a>');
	
	    return replacedText
	},
	
	addChatNotification: function(body, extraClass) {
		var $chatArea = $('#inner-chat-area');
		
		var $message = $('<div class="chat-message notification alert alert-info"></div>');
		if (extraClass) {
			$message.addClass(extraClass);
		}
		
		var date = new Date();
		var timeStamp = (date.getHours() < 10 ? '0' : '') + date.getHours() + ':' +
			(date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
		
		$message.append(
			'<span class="msg-body">(' + timeStamp + ') ' + body + '</span>'
		);
		
		$chatArea.append($message);
		WGchat_API.scrollChat();
	},
	
	scrollChat: function(reset) {
		var $chatArea		= $('#chat-area');
		var $innerChatArea 	= $('#inner-chat-area');
		
		WGchat_API.resizeChatArea();
		
		if (reset == true) {
			$chatArea.scrollTop(0);
			//$innerChatArea.css({ marginTop: 0 });	
		} else {
			var chatHeight 	= $chatArea.height() - 11;
			var innerHeight = $innerChatArea.height();
			var delta = innerHeight - chatHeight;
			
			if (delta > 0) {
				$chatArea.scrollTop(delta);	
			}	
		}
	},
	
	buildForm: function(fields) {
		// console.log('FORM');
		
		var $formTag = $('<form action="#"></form>');
		
		// Check value from cookie, if there is a value in cookie, don't show the field
		// auto assign the value to corresponding field.
		var cookieData = WGchat_Backend.getDataCookie();
		
		// Checking the userdata value
		if (WGchat_GLOBAL().userData !== null) {
			$.each(WGchat_GLOBAL().userData, function(dKey, dValue) {
				if (!cookieData || !cookieData[dKey]) {
		        	var newData = {};
		        	newData[dKey] = dValue;
		        	
		        	WGchat_Backend.storeDataCookie(newData);					
				}
	        });
	        
	        cookieData = WGchat_Backend.getDataCookie();
		}
		
		$.each(fields, function(fieldidx, field) {
			if (cookieData !== null) {
				var aData = cookieData[field.id];
			}
			
			var $div = $('<div class="control-group"></div>');
			var requiredString = '';
			
			if (field.required) {
				requiredString = '&nbsp;<span class="label label-important">required</span>';
			}
			
			$div.append(
				'<label for="' + field.id + '"><h4 style="display: inline;">' + field.label + '</h4>' + requiredString + '</label>'
			);
			
			var storedValue = undefined;
			if (aData !== undefined && aData !== null && $.trim(aData.toString()) !== '') {
				storedValue = cookieData[field.id];
			}
			
			if (field.type === 'text-single') {
				var $container = $div.append(
					'<input type="text" id="' + field.id + '" name="' + field.id + '" />'
				);
				
				$container.find('input').attr('value', storedValue);
			} else {
				$.each(field.values, function(validx, value) {
					var $container = $div.append(
						'<input type="radio" id="' + field.id + '" name="' + field.id + '" value="' + value + '"/>' +
						'&nbsp;' + value + '&nbsp;&nbsp;'
					);
					
					if (storedValue === value) {
						$container.find('input').attr('checked', true);
					}
				});
			}
			
			$formTag.append($div);
		});
		
		return $formTag;
	},
	
	validateForm: function(fields) {
		var $scope = $('.control-group');
		var isValid = true;
		
		$.each(fields, function(fieldidx, field) {
			var id = field.id;
			var required = field.required;
			var value = undefined;
			
			var $input = $scope.find('input[name=' + id + ']');
			if (field.type === 'text-single') {
				if ($.trim($input.val().toString()).length > 0) {
                    value = $.trim($input.val().toString());
                }
			} else {
				var $input = $scope.find('input[name=' + id + ']:checked');
                
                if ($input.length > 0) {
                    value = $input.val();
                }
			}
			
			// Check if the field is required
            $scope.find('label[for=' + id + ']').removeClass('form-error');
            
            if (required && value === undefined) {
                isValid = false;
                $scope.find('label[for=' + id + ']').addClass('form-error');
            } else if (!required && value === undefined) {
                value = '';
            }
            
            // append value to varFields
            field.value = value;
		});
		
		return isValid;
	},
	
	sendSurveyForm: function(fields, sessionId) {
		WGchat_API.setFormVisible(false);
		WGchat_API.setLoaderVisible(true, 'Sending your answer(s)...');
		
		// TODO: Must initiate new connection, only for sending survey answer!
		WGchat_Backend.openConnection(function(status, msg) {
			if (status === Strophe.Status.CONNECTED) {
	       		var req = SurveyAnswer.build(WGchat_Backend.connection.jid, WGchat_API.workgroup, sessionId, fields);
		        var id = $(req.tree()).attr('id');
		        
		        WGchat_Backend.connection.addHandler(function(packet) {
		        	WGchat_API.setLoaderVisible(false);
		        	WGchat_API.setChatVisible(true, true, false);
		        	
		        	if (_gaq) {
						_gaq.push(['_trackPageview',  AnalyticsPagePath.surveyEnd]);
						_gaq.push(['acc2._trackPageview', AnalyticsPagePath.surveyEnd]);
					}
					
					WGchat_Backend.closeConnection();
		        }, null, null, null, id);
		        WGchat_Backend.send(req);     
	        } else if (status === Strophe.Status.CONNFAIL) {
	        	// exit gracefully, don't send any log when faild to connect for sending survey answer
	        	WGchat_API.setLoaderVisible(false);
	        	WGchat_API.setChatVisible(true, true, false);
	        }
	        
	        WGchat_Backend.resetCookie();
		});
		
		WGchat_Backend.resetCookie();
	},
	
	sendForm: function(fields, storedUserId) {
		WGchat_API.setFormVisible(false);
		WGchat_API.setLoaderVisible(true, 'Contacting customer services...');
		
		// Get userID from fields email, if not exist fallback to WGchat_GLOBAL().userID;
		var tempId = WGchat_GLOBAL().userID;
		for(var i = 0, j = fields.length; i < j; i++){
        	var checkedField = fields[i];
        	if (checkedField.id === 'email') {
        		tempId = checkedField.value;
        	}
        };
        
		var userId = (storedUserId !== undefined) ? storedUserId : tempId;
        var wgJid = WGchat_API.workgroup;
        
        // Check for facebook data
        if (WGchat_GLOBAL().userData !== null) {
        	userId = WGchat_GLOBAL().userData.id;	
        }
        
        // Append few additional field
        fields.push({
            id: 'userID',
            value: userId
        });
        
        // Push the transportType
        fields.push({
        	id: 'TransportType',
        	value: 'widget'
        });
        
        // Store fields to cookie
        $.each(fields, function(fieldidx, field) {
        	var newData = {};
        	newData[field.id] = field.value;
        	
        	WGchat_Backend.storeDataCookie(newData);
        });
        
        var parentLocation = WGchat_API.location;
        if (WGchat_GLOBAL().userData !== null) {
        	parentLocation = WGchat_GLOBAL().userData.location;
        } else if (parentLocation === undefined) {
        	parentLocation = window.location.href;
        }
        
        fields.push({
            id: 'location',
            value: parentLocation
        });
        
        if ($.trim(WGchat_GLOBAL().channelId.toString()).length > 0) {
        	fields.push({
        		id: 'channel',
        		value: WGchat_GLOBAL().channelId
        	});
        }
        
        var req = JoinQueue.build(WGchat_Backend.connection.jid, wgJid, userId, fields);
        id = $(req.tree()).attr('id');
        
        WGchat_Backend.connection.addHandler(WGchat_API.handleQueueStatus, 'http://jabber.org/protocol/workgroup', 'message');
        WGchat_Backend.connection.addHandler(WGchat_API.handleInvitation, 'jabber:x:conference', 'message');
        WGchat_Backend.send(req);
        
        if (_gaq) {
			_gaq.push(['_trackPageview',  AnalyticsPagePath.waitingAgent]);
			_gaq.push(['acc2._trackPageview', AnalyticsPagePath.waitingAgent]);
		}
	},
	
	resizeChatArea: function(hiddenTextArea) {
		setTimeout(function() {
			var chatArea = $('#chat-area');
			var inputArea = $('#chat-input-area');
			var footerArea = $('#footer')
			var headerArea = $('#header');
			
			var chatHeight = $(window).height() - (hiddenTextArea ? 0 : inputArea.height()) - footerArea.height() - (headerArea.height() + 11);
			chatArea.height(chatHeight - 27);
		}, 250);
	},
	
	updateUnreadMessage: function(unreadMessage) {
		WGchat_API.unreadMessage = unreadMessage;
		
		var suffix = '';
		if (unreadMessage > 0) {
			suffix = '(' + unreadMessage + ') ';
		}
		
		windowProxy.post({ type: 'updateHeader', data: {
			headerText: suffix + WGchat_GLOBAL().widgetTitle
		} });
	},
	
	updateConnectionCookie: function() {
		if (WGchat_Backend.connection && WGchat_API.roomjid) {
			var curDate = new Date();
			var expDate = new Date(curDate.getTime() + WGchat_API.cookieTimeout);
			
			$.cookie('wgchat-roomjid', WGchat_API.roomjid, { expires: expDate, path: location.pathname });
	        $.cookie('wgchat-jid', WGchat_Backend.connection.jid, { expires: expDate, path: location.pathname });
	        $.cookie('wgchat-sid', WGchat_Backend.connection.sid, { expires: expDate, path: location.pathname });
	        $.cookie('wgchat-rid', WGchat_Backend.connection.rid, { expires: expDate, path: location.pathname });
    	}
	},
	
	/**
	 * set the wgchat-interaction cookie for specified path
	 */
	userInteraction: function() {
		var curDate = new Date();
		var expDate = new Date(curDate.getTime() + WGchat_API.cookieTimeout);
		
		$.cookie('wgchat-interaction', '1', { expires: expDate, path: location.pathname });
	},
	
	/**
     * determine whether the user already has a form data
     */
    isNewComer: function() {
    	return $.cookie('wgchat-interaction') === null;
    },
};

// Setup soundManager
// Below is prone to error!
// soundManager.url = WGchat_GLOBAL().baseUrl + 'assets/js/soundmanager/swf/';
// soundManager.onready(function() {
	// soundManager.createSound({
		// id: 'incomingchat',
		// url: WGchat_GLOBAL().baseUrl + '/assets/sounds/incoming.wav'
	// });
// });

$(document).ready(function() {
	if (WGchat_GLOBAL().isWidget) {
		Porthole.WindowProxyDispatcher.start();
	} else {
		WGchat_API.location = WGchat_GLOBAL().parentUrl;
	}
	
	WGchat_API.onlineWorkgroupCheck();
	
	// Get real windows Location
	if (windowProxy) {
		windowProxy.post({ type: 'getLocation', data: { } });
	}
	
	// Handle window resize event
	var resizeTimer;
	$(window).resize(function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(WGchat_API.resizeChatArea, 250);
	});
	
	$(window).on('beforeunload', function() {
		if (WGchat_Backend.connection) {
			WGchat_Backend.connection.flush();
			WGchat_Backend.connection.pause();
			
			WGchat_API.updateConnectionCookie();
		}
	});
	
	var resizeIntervalTimer = setInterval(function() {
		if (!WGchat_API.isHidden) {
			WGchat_API.resizeChatArea();
		}
	}, 3000);
	
	// Initial resize
	WGchat_API.resizeChatArea();
	
	var header_background	= WGchat_GLOBAL().colorScheme.header_background;
	var header_color		= WGchat_GLOBAL().colorScheme.header_text_color;	
	var borderColor			= (new Color(header_background)).getDarker(30);
	
	// Change color via colorScheme
	$('#header').css({
		backgroundColor: header_background,
		color: header_color,
		borderBottom: '1px solid ' + borderColor.toString()
	});
	
	$('#chat-input-area').css({
		backgroundColor: header_background
	});
});